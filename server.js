/**
	Created by rahilvora on 07/30/17
*/

'use strict';
require('./utils/rootRequire')();
require('./utils/prodEnv')();
let express = require('express');
let http    = require('http');
var cors = require('cors');
let Router  = rootRequire('app/Router');
let app     = express();
let server  = http.createServer(app);
let app_config = rootRequire('config/app_config');
let mysqlEventEmitter = rootRequire('./utils/mysqlEventEmitter');
let session = require('express-session');
let RedisStore = require('connect-redis')(session);
let taskScheduler = rootRequire('app/controllers/taskControllers/TaskScheduler');

let mongodb = rootRequire('config/mongodb');

//Configure middleware ()

// // socket available globally
// app.set('chat_socket', io);

// To support CORS capability
app.use(cors());
/**
 * SESSION
 ********************* */
app.use(session({
  store: new RedisStore({
    host: '127.0.0.1',
    port: 8000,
    prefix:'ridr-sess'
  }),
  resave: false,
  secret: 'ridr',
  saveUninitialized: false
}));

let checkUtil = rootRequire('utils/checkUtil');

/**
 * Environment variables
 */
require('dotenv').config();

/**
* MIDDLEWARE
********************* */
require('./config/middleware')(app, express);
/**
* ROUTES
********************* */
app.use(rootRequire('utils/flash'));

server.listen(app_config.APP_PORT);

//On connecting to a socket perform required configuration setting
Router.forEach(route => {
  app.use(route.path, route.handler);
});

app.use((req, res, next) => {
  res.status(404);
  res.render('global/404', {
    title: 'Page introuvable !',
  });
});

app.param('user_id', function(req, res, next, user_id) {
  // typically we might sanity check that user_id is of the right format
  console.log('Found userid id as '+ user_id);
  /*if (!checkUtil.isNotEmpty(group_id)) {
   let err = new Error();
   err.status = 404;
   return next(err);
   }*/
  req.user_id = user_id;
  next();
});
app.param('group_id', function (req, res, next, group_id) {
  console.log('Found group id as '+ group_id);
  if (!checkUtil.isNotEmpty(group_id)) {
    let err = new Error();
    err.status = 404;
    return next(err);
  }
  req.group_id = group_id;
  next();

});

taskScheduler.initAllSchedules(app.request);

console.log('Server started on localhost: ' + app_config.APP_PORT);