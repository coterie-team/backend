/**
	Created by rahilvora on 07/30/17
*/

'use strict';

let express    = require('express');
let controller = rootRequire('app/controllers/taskControllers/TaskController');
let router     = express.Router();

router.get('/show', controller.showTasks.get);
router.post('/new', controller.addTaskForGroup.post);
router.put('/edit/:task_id', controller.editTask.put);
router.delete('/delete/:task_id', controller.deleteTask.delete);
router.get('/users-for-task/:task_id', controller.usersInvolvedInTask.get);

module.exports = router;