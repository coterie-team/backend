/**
 Created by rahilvora on 07/30/17
 */

'use strict';

let express = require('express');
let Controller = rootRequire('app/controllers/userControllers/UserController');
let passport = require('passport');
let router = express.Router();



router.get('/signup', Controller.signup.get);
router.post('/register', Controller.register.post);
router.post('/check-login', Controller.checkLogin.post);
router.get('/login', Controller.login.get);
router.post('/login', Controller.login.post);
router.post('/verify-email', Controller.verifyEmail.post);
router.get('/:id/find-profile-by-id', Controller.findProfileById.get);
router.get('/:first_name/:last_name/find-profile-by-name', Controller.findProfileByName.get);
router.get('/:email/find-profile-by-email', Controller.findProfileByEmail.get);
router.get('/search-user/:term', Controller.searchUser.get);
router.put('/update-profile/:user_id/', Controller.updateUserProfile.put);
router.put('/:id/update-password', Controller.updatePassword.put);
// router.delete('/:id/', Controller.id.delete);
router.get('/tasks/:user_id', Controller.tasks.get);
router.get('/groups/:user_id', Controller.groups.get);

router.get('/google', passport.authenticate('google', {scope: ['profile', 'email']}));
router.get('/google/home',
    passport.authenticate('google', {
        successRedirect: '/home',
        failureRedirect: '/login'
    }));

router.get('/facebook', passport.authenticate('facebook', {scope: 'email'}));
router.get('/facebook/signup',
    passport.authenticate('facebook', {
        successRedirect: '/home',
        failureRedirect: '/login'
    }));

module.exports = router;