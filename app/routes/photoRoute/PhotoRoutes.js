/**
	Created by rahilvora on 07/30/17
*/

'use strict';

let express    = require('express');
let controller = rootRequire('app/controllers/photoController/PhotoController');
let router     = express.Router();

router.post('/upload-user/:user_id', controller.uploadUserImage.post);
router.post('/upload-group/:group_id', controller.uploadGroupImage.post);
router.get('/load/:file_id', controller.loadImage.get);
router.delete('/delete/:file_id', controller.deleteImage.delete);


module.exports = router;