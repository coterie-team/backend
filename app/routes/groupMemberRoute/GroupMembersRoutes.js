/**
 Created by rahilvora on 08/28/17
 */

'use strict';

let express    = require('express');
let Controller = rootRequire('app/controllers/groupMemberControllers/GroupMemberController');
let router     = express.Router();

router.get('/:group_id', Controller.showAllMembers.get);
router.post('/:group_id', Controller.addMember.post);
router.get('/:member_id/getGroupsForMembers', Controller.showAllGroups.get);
router.put('/:member_id', Controller.editMember.put);
router.delete('/:group_id/:member_id', Controller.deleteMember.delete);

module.exports = router;