/**
 * Created by rishi on 11/8/17.
 */

'use strict';

let express    = require('express');
let controller = rootRequire('app/controllers/notificationController/NotificationController');
let router     = express.Router();

router.post('/save', controller.saveRegistrationTokenForUser.post);
router.post('/test-web-notification', controller.testNotificationWeb.post);
router.get('/test-send', controller.testNotification.get);
router.get('/test-send-user/:user_id/:task_id', controller.testNotificationForUserIdAndTaskId.get);
router.get('/test-send-group/:group_id/:task_id', controller.testNotificationForGroupIdAndTaskId.get);

module.exports = router;