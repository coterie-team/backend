/**
	Created by rahilvora on 07/30/17
*/

'use strict'

let express    = require('express');
let Controller = rootRequire('app/controllers/billsControllers/BillsController');
let router     = express.Router();

router.get('/', Controller.index.get);
router.get('/new', Controller.new.get);
router.post('/new', Controller.new.post);

module.exports = router;