/**
	Created by rahilvora on 07/30/17
*/

'use strict';

let express    = require('express');
let Controller = rootRequire('app/controllers/groupsControllers/GroupController');
let router     = express.Router();

router.get('/:user_id', Controller.getUserGroups.get);
router.post('/add-group', Controller.addGroup.post);
//router.post('/new', Controller.new.post);

router.put('/update/:group_id', Controller.updateGroup.put);
router.get('/:group_id/getDetails', Controller.getGroupDetails.get);
router.delete('/delete/:group_id', Controller.deleteGroup.delete);

module.exports = router;