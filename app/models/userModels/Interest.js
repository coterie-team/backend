/**
 * Created by rishi on 8/28/17.
 */

'use strict';
let db   = rootRequire('config/db');
let User = require('../userModels/User');

//Define database table for Task Module
let Interest = db.define('interest', {
        activity:{
            type: db._Sequelize.STRING,
            primaryKey: true,
            allowNull: false
        },
        user_id:{
            type: db._Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            references: {
                model: User,
                key: 'id',
            }
        }
    },
    {
        freezeTableName: true
    }
);

module.exports = Interest;