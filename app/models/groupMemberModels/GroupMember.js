/**
 * Created by rahilvora on 8/28/17.
 */

'use strict';
let db   = rootRequire('config/db');
let UserGroup = require('../groupModels/UserGroup');
let User = require('../userModels/User');

const GroupMember = db.define('group_member',{
        user_id:{
            type: db._Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            references:{
                model:User,
                key:'id'
            }
        },
        group_id:{
            type: db._Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            references:{
                model:UserGroup,
                key:'id'
            }
        }
    },
    {
        freezeTableName: true
    }
);

module.exports = GroupMember;