/**
 * Created by rishi on 8/28/17.
 */

'use strict';
let db   = rootRequire('config/db');
let UserGroup = require('../groupModels/UserGroup');
let User = require('../userModels/User');
let Task = require('./Task');

//Define database table for Task Module

let TaskAssignedTo = db.define('task_assigned_to', {
        assigned_date: {
            type: db._Sequelize.DATE,
            allowNull: false,
            defaultValue: db._Sequelize.NOW
        },
        due_date: {
            type: db._Sequelize.DATE,
            allowNull: false,
            defaultValue: db._Sequelize.NOW
        },
        completed: {
            type: db._Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        assigned_user_id: {
            type: db._Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: User,
                key: 'id',
            }
        },
        group_id: {
            type: db._Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: UserGroup,
                key: 'id',
            }
        },
        task_id: {
            type: db._Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: Task,
                key: 'id',
            }
        },
        assign_seq: {
            type: db._Sequelize.INTEGER,
            allowNull: false
        }
    },
    {
        freezeTableName: true
    }
);

module.exports = TaskAssignedTo;