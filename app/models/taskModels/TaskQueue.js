/**
 * Created by Ishan on 10/1/2017.
 */

'use strict';
let db = rootRequire('config/db');
let Task = require('../taskModels/Task');
let Group = require('../groupModels/UserGroup');
let User = require('../userModels/User');
//Define database table for TaskQueue Module

let TaskQueue = db.define('task_queue', {
        id: {
            type: db._Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        group_id: {
            type: db._Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: Group,
                key: 'id'
            }
        },
        user_id: {
            type: db._Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: User,
                key: 'id'
            }
        },
        task_id: {
            type: db._Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: Task,
                key: 'id'
            }
        },
        user_seq: {
            type: db._Sequelize.INTEGER,
            allowNull: false
        }
    },
    {
        freezeTableName: true
    }
);


module.exports = TaskQueue;
