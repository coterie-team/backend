/**
 Created by rahilvora on 07/30/17
 */

'use strict';
let db   = rootRequire('config/db');
let UserGroup = require('../groupModels/UserGroup');
//Define database table for Task Module

let Task = db.define('task', {
        id: {
        	type: db._Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true,
            allowNull: false
		},
        name: {
        	type: db._Sequelize.STRING,
			allowNull: false
		},
        frequency: {
        	type: db._Sequelize.STRING,
			allowNull: false
		},
        start_date: {
        	type: db._Sequelize.DATE,
			allowNull: false,
			defaultValue: db._Sequelize.NOW
		},
        type: {
        	type: db._Sequelize.STRING,
			allowNull: false
		},
        group_id: {
            type: db._Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: UserGroup,
                key: 'id',
            }
        },
        assign_type: {
            type: db._Sequelize.STRING,
            allowNull: false,
            defaultValue: 'Alphabetical'
        },
        schedule_id: {
            type: db._Sequelize.STRING,
            allowNull: false,
            defaultValue: '-1'
        },
        added_by: {
            type: db._Sequelize.INTEGER,
            allowNull: false
        }
    },
    {
        freezeTableName: true
    }
);


module.exports = Task;
