/**
	Created by rahilvora on 07/30/17
*/

'use strict';
let db   = rootRequire('config/db');

//Define database table for Group Module

let UserGroup = db.define('user_group', {
		id:{
			type: db._Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		name:{
			type: db._Sequelize.STRING,
			allowNull: false
		},
		photo_id:{
			type: db._Sequelize.STRING,
			unique: true,
			allowNull: true
		},
		members: {
			type: db._Sequelize.INTEGER,
			allowNull: false,
			defaultValue: 0
		},
		added_by: {
			type: db._Sequelize.INTEGER,
			allowNull: false
		},
		chat_space: {
			type: db._Sequelize.STRING,
			allowNull: true,
            defaultValue: null
		}
	},
    {
        freezeTableName: true
    }
);
module.exports = UserGroup;

