/**
 * Created by rishi on 11/8/17.
 */

'use strict';

let User = rootRequire('app/models/userModels/User');
let GroupMember = rootRequire('app/models/groupMemberModels/GroupMember');
let Task = rootRequire('app/models/taskModels/Task');

let firebase_admin = rootRequire('config/app_firebase');
let checkUtil = rootRequire('utils/checkUtil');
require('isomorphic-fetch');
module.exports = {
    testNotification: {
        get(req, res){
            // Fetch tokens for the users in the specific group and send notifications
            var registrationToken = "dlpaaIAgJAg:APA91bHvNorj3K9W__VKnmHhFap-MBqM97GGd1J3ebNKkneisuqULSn3xXRIfSSCKSY5wQtFQocHC4OF6QibUPSSX8lbbEQSl3VMhsLADCBsbii9Wcmx0yOpwiRZJSHR4TQkCXiIOki6";
            var payload = {
                notification: {
                    title: "Coterie",
                    body: "Your turn starts tomorrow 8 am."
                }
            };

            firebase_admin.messaging().sendToDevice(registrationToken, payload)
                .then(function (response) {
                    console.log("Successfully sent message:", response);
                    let api_response = {
                        status_code: 200,
                        msg: "Successfully sent message",
                        data: response
                    };
                    res.send(api_response);
                })
                .catch(function (error) {
                    console.log("Error sending message:", error);
                    let api_response = {
                        status_code: 400,
                        msg: "Error sending message " + error,
                        data: {}
                    };
                    res.send(api_response);
                });
        }
    },
    testNotificationWeb: {
      post(req, res){
          let key = 'AAAA7Aq0HSA:APA91bG-3DMuQ5WlRA6ZiO8wAPmc4k2gDP9eYoCe_C3pTIypjJqnIjbZdWPMj8ym4v_rj6aQs9LStieUGlntQ4cJjQJyYLFWtGXNeefMyJv5XIGa73QjCuP8QeW6FzDUE0cQjOYK1Jy8';
          let to = 'ev0AE8Lqy50:APA91bFRTGCWtlmvuOvPES0_JavJf89uWEEwRZ5640fzQphdFHUXVykJEpxX33FzgjO4rPAE_EyEz9LmJ7Hb7KXld2tJV8ASU1YZ8xI3kjvqMXSG05oI7_R9wJJxVA4FitJqJZHsMDUA';
          let notification = {
              'title': 'Portugal vs. Denmark',
              'body': '5 to 1',
              'icon': 'firebase-logo.png',
              'click_action': 'http://localhost:8081'
          };

          fetch('https://fcm.googleapis.com/fcm/send', {
              'method': 'POST',
              'headers': {
                  'Authorization': 'key=' + key,
                  'Content-Type': 'application/json'
              },
              'body': JSON.stringify({
                  'notification': notification,
                  'to': to
              })
          }).then(function(response) {
              console.log(response);
              res.sendStatus(200,{})
          }).catch(function(error) {
              console.error(error);
          })
      }
    },

    testNotificationForUserIdAndTaskId: {
        get(req, res){
            // Fetch tokens for the users in the specific group and send notifications
            if (checkUtil.isEmpty(req.params.user_id) || checkUtil.isEmpty(req.params.task_id)) {
                res.send({status_code: 400, msg: 'No user_id or task_id found, bad request', data: {}});
                return;
            }

            User.findOne(
                {
                    where: {id: req.params.user_id}
                }
            ).then(user_data => {

                Task.findOne({
                    where: {id: req.params.task_id}
                }).then(task => {
                    var payload = {
                        notification: {
                            title: "Coterie",
                            body: "Your turn for " + task.frequency + " task of " + task.name + " starts tomorrow."
                        },
                        data: {
                            task_name: task.name,
                            task_frequency: task.frequency
                        }
                    };

                    sendNotificationForToken(user_data.fcm_token, payload)
                        .then(response => {
                            if (response) {
                                let api_response = {
                                    status_code: 200,
                                    msg: "Successfully sent message",
                                    data: response
                                };
                                res.send(api_response);
                            } else {
                                let api_response = {
                                    status_code: 400,
                                    msg: "Error sending message",
                                    data: {}
                                };
                                res.send(api_response);
                            }
                        }).catch(err => {
                            console.log('Notification send failed ' + err);
                            const response = {status_code: 503, msg: 'Failed to send notification, maybe FCM token is not present', data: []};
                            res.send(response);
                    });
                });
            }).catch(err => {
                const response = {status_code: 503, msg: 'Error fetching user data', data: []};
                res.send(response);
            });
        }
    },

    testNotificationForGroupIdAndTaskId: {
        get(req, res){

            if (checkUtil.isEmpty(req.params.group_id) || checkUtil.isEmpty(req.params.task_id)) {
                res.send({status_code: 400, msg: 'No group_id or task_id found, bad request', data: {}});
                return;
            }

            GroupMember.findAndCountAll({
                where: {
                    group_id: req.params.group_id
                },
                include: [
                    {
                        model: User
                    }
                ]
            }).then(groupMembers => {
                Task.findOne({
                    where: {id: req.params.task_id}
                }).then(task => {

                    let registrationTokens = [];
                    for (let key in groupMembers.rows) {
                        if (groupMembers.rows.hasOwnProperty(key) && checkUtil.isNotEmpty(groupMembers.rows[key].user.fcm_token)) {
                            registrationTokens.push(groupMembers.rows[key].user.fcm_token);
                        }
                    }
                    var payload = {
                        notification: {
                            title: "Coterie",
                            body: "Turn changes tomorrow 8 am"
                        },
                        data: {
                            task_name: task.name,
                            task_frequency: task.frequency
                        }
                    };

                    sendNotificationForToken(registrationTokens, payload)
                        .then(response => {
                            if (response) {
                                let api_response = {
                                    status_code: 200,
                                    msg: "Successfully sent message",
                                    data: response
                                };
                                res.send(api_response);
                            } else {
                                let api_response = {
                                    status_code: 400,
                                    msg: "Error sending message",
                                    data: {}
                                };
                                res.send(api_response);
                            }
                        }).catch(err => {
                            console.log('Notification send failed ' + err);
                            const response = {status_code: 503, msg: 'Failed to send notification, maybe FCM token is not present', data: []};
                            res.send(response);
                    });
                });
            }).catch(err => {
                const response = {status_code: 503, msg: 'Error fetching group data', data: []};
                res.send(response);
            });
        }
    },

    saveRegistrationTokenForUser: {
        post(req, res){
            let body = req.body;

            if (checkUtil.isEmpty(body)) {
                res.send({status_code: 400, msg: 'No message body found, bad request', data: {}});
                return;
            }

            let token = body.token;
            let user_id = req.session.user_id;

            if (checkUtil.isEmpty(user_id)) {
                let response = {
                    status_code: 400,
                    msg: "No user_id found in session",
                    data: {}
                };
                res.send(response);
                return;
            }

            User.update(
                {
                    fcm_token: token
                },
                {
                    where: {id: user_id}
                })
                .then(affectedRows => {
                    var status_code = 200;
                    var msg = "Registration token saved in successfully for user_id " + user_id;
                    if (affectedRows != 1) {
                        status_code = 400;
                        msg = "Failed to save the registraion token"
                    }
                    let response = {
                        status_code: status_code,
                        msg: msg,
                        data: {}
                    };
                    res.send(response);
                })
                .catch(error => {
                    let response = {
                        status_code: 500,
                        msg: "Error saving registration token",
                        data: {}
                    };
                    res.send(response);
                });

        }

    },

    // sendNotificationToUser(user_id, payload) {
    //     User.findOne({
    //         where: {id: user_id}
    //     }).then(user => {
    //         firebase_admin.messaging().sendToDevice(user.fcm_token, payload)
    //             .then(function (response) {
    //                 console.log("Successfully sent message:", response);
    //             })
    //             .catch(function (error) {
    //                 console.log("Error sending message:", error);
    //             });
    //     });
    // },

    sendTaskReminderNotification(user_id, task_id) {
        return new Promise(
            function (resolve, reject) {
                User.findOne({
                    where: {id: user_id}
                }).then(user => {
                    Task.findOne({
                        where: {id: task_id}
                    }).then(task => {
                        console.log('Sending notification to user_id ' + user.id);
                        if(checkUtil.isEmpty(user.fcm_token)) {
                            console.log('FCM Token not found for the user, so returning ');
                            return;
                        }
                        let payload = {
                            notification: {
                                title: "Coterie",
                                body: "Your turn for " + task.frequency + " task of " + task.name + " starts tomorrow."
                            },
                            data: {
                                task_name: task.name,
                                task_frequency: task.frequency
                            }
                        };
                        firebase_admin.messaging().sendToDevice(user.fcm_token, payload)
                            .then(function (response) {
                                console.log("Successfully sent message:", response);
                                resolve(response);
                            })
                            .catch(function (error) {
                                console.log("Error sending message:", error);
                                reject(null);
                            });
                    })
                });
            }
        )
    },

    sendTaskCreateNotification(members_arr, task) {
        for(let i=0; i < members_arr.length; i++) {
            User.findOne({
                where: {id: task.added_by}
            }).then(user => {
                for(var i=0; i<members_arr.length; i++) {
                    let payload = {
                        notification: {
                            title: "Coterie",
                            body: user.first_name + " created new " + task.frequency + " task for " + task.name
                        },
                        data: {
                            task_name: task.name
                        }
                    };
                    sendNotificationToUser(members_arr[i], payload);
                }
            });
        }
    },

    sendGroupCreateNotification(members_arr, user_group) {
        for(let i=0; i < members_arr.length; i++) {
            User.findOne({
                where: {id: user_group.added_by}
            }).then(user => {
                for(var i=0; i<members_arr.length; i++) {
                    let payload = {
                        notification: {
                            title: "Coterie",
                            body: user.first_name + " added you to " + user_group.name + " group."
                        },
                        data: {
                            user_group: user_group.name
                        }
                    };
                    sendNotificationToUser(members_arr[i], payload);
                }
            });
        }
    }
};

function sendNotificationForToken(registrationTokens, payload) {
    return new Promise(
        function (resolve, reject) {
            firebase_admin.messaging().sendToDevice(registrationTokens, payload)
                .then(function (response) {
                    console.log("Successfully sent message:", response);
                    resolve(response);
                })
                .catch(function (error) {
                    console.log("Error sending message:", error);
                    reject(null);
                });
        }
    )
}

function sendNotificationToUser(user_id, payload) {
    User.findOne({
        where: {id: user_id}
    }).then(user => {
        firebase_admin.messaging().sendToDevice(user.fcm_token, payload)
            .then(function (response) {
                console.log("Successfully sent message:", response);
            })
            .catch(function (error) {
                console.log("Error sending message:", error);
            });
    });
}