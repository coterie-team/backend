/**
 * Created by rishi on 11/21/17.
 */

'use strict';

let User = rootRequire('app/models/userModels/User');
let UserGroup = rootRequire('app/models/groupModels/UserGroup');
let MulterUpload = rootRequire('config/app_multer');
var mongoose = require('mongoose');

let Grid = require("gridfs-stream");
Grid.mongo = mongoose.mongo;
let conn = mongoose.connection;
let gfs;

conn.once('open', function () {
    gfs = Grid(conn.db);
    console.log('GFS created successfully!!');
});

let checkUtil = rootRequire('utils/checkUtil');

var fs = require("fs");

module.exports = {
    uploadUserImage: {
        post(req, res){
            var upload = MulterUpload.single('photo');
            upload(req, res, function (err) {
                if (err) {
                    console.log('Error uploading the image using Multer ' + err);
                    res.send({status_code: 400, msg: 'Error uploading the image ' + err, data: {}});
                    return;
                }

                if (checkUtil.isEmpty(req.file) || checkUtil.isEmpty(req.params.user_id)) {
                    res.send({status_code: 400, msg: 'No file or user_id found', data: {}});
                    return;
                }

                let user_id = req.params.user_id;
                let fileId = mongoose.Types.ObjectId();
                let originalFilename = req.file.originalname;
                let filename = req.file.filename;

                console.log('Uploading photo with _id ' + fileId + '  and name ' + filename);
                var writestream = gfs.createWriteStream({
                    _id: fileId,
                    filename: originalFilename
                });

                writestream.on('close', function (file) {
                    // do something with `file`
                    console.log('Writing to file is done .........' + file.filename);
                });

                //pipe multer's temp file /uploads/filename into the stream we created above. On end deletes the temporary file.
                fs.createReadStream("./uploads/" + filename)
                    .on("end", function () {
                        fs.unlink("./uploads/" + filename, function (err) {
                            User.update({photo_url: fileId.toString()}, {where: {id: user_id}}).then(result => {
                                User.findOne({where: {id: user_id}}).then(updatedUser => {
                                    res.send({
                                        status_code: 200,
                                        msg: 'Photo uploaded successfully for the user!',
                                        data: updatedUser
                                    });
                                });
                            });
                        })
                    })
                    .on("err", function () {
                        res.send({status_code: 400, msg: 'Photo upload failed, try again.', data: {}});
                    })
                    .pipe(writestream);
            });
        }
    },

    uploadGroupImage: {
        post(req, res){
            var upload = MulterUpload.single('photo');
            upload(req, res, function (err) {
                if (err) {
                    console.log('Error uploading the image using Multer ' + err);
                    res.send({status_code: 400, msg: 'Error uploading the image ' + err, data: {}});
                    return;
                }

                if (checkUtil.isEmpty(req.file) || checkUtil.isEmpty(req.params.group_id)) {
                    res.send({status_code: 400, msg: 'No file or user_id found', data: {}});
                    return;
                }

                let group_id = req.params.group_id;
                let fileId = mongoose.Types.ObjectId();
                let originalFilename = req.file.originalname;
                let filename = req.file.filename;

                console.log('Uploading photo with _id ' + fileId + '  and name ' + filename);
                var writestream = gfs.createWriteStream({
                    _id: fileId,
                    filename: originalFilename
                });

                writestream.on('close', function (file) {
                    // do something with `file`
                    console.log('Writing to file is done .........' + file.filename);
                });

                //pipe multer's temp file /uploads/filename into the stream we created above. On end deletes the temporary file.
                fs.createReadStream("./uploads/" + filename)
                    .on("end", function () {
                        fs.unlink("./uploads/" + filename, function (err) {
                            UserGroup.update({photo_id: fileId.toString()}, {where: {id: group_id}}).then(result => {
                                UserGroup.findOne({where: {id: group_id}}).then(updateGroup => {
                                    res.send({
                                        status_code: 200,
                                        msg: 'Photo uploaded successfully for the group!',
                                        data: updateGroup
                                    });
                                });
                            });
                        })
                    })
                    .on("err", function () {
                        res.send({status_code: 400, msg: 'Photo upload failed, try again.', data: {}});
                    })
                    .pipe(writestream);
            });
        }
    },

    loadImage: {
        get(req, res){
            if (checkUtil.isEmpty(req.params.file_id)) {
                res.send({status_code: 400, msg: 'Bad request, no file_id value', data: {}});
                return;
            }
            // let fileId = req.params.file_id + '';
            let fileId = req.params.file_id;
            console.log('Loading image file_id ' + fileId);
            var readstream = gfs.createReadStream({_id: mongoose.Types.ObjectId(fileId)});
            readstream.on("error", function (err) {
                res.send({status_code: 404, msg: 'No image found with file_id ' + err, data: {}});
            });
            readstream.pipe(res);
        }
    },

    deleteImage: {
        delete(req, res) {
            if (checkUtil.isEmpty(req.params.file_id)) {
                res.send({status_code: 400, msg: 'Bad request, no file_id found', data: {}});
                return;
            }
            let fileId = req.params.file_id;

            console.log('Deleting the photo with id ' + fileId);
            gfs.exist({_id: mongoose.Types.ObjectId(fileId)}, function (err, found) {
                if (err) {
                    res.send({status_code: 404, msg: 'Error finding the image with file_id ' + err, data: {}});
                    return;
                }
                if (found) {
                    gfs.remove({_id: mongoose.Types.ObjectId(fileId)}, function (err) {
                        if (err) {
                            res.send({status_code: 400, msg: 'Error removing the image with file_id ' + err, data: {}});
                            return;
                        }
                        res.send({status_code: 200, msg: 'Image deleted successfully!', data: {}});
                    });
                } else {
                    res.send({status_code: 404, msg: 'No image found with file_id ' + err, data: {}});
                }
            });
        }
    }
};


