/**
 * Created by rahilvora on 8/28/17.
 */

'use strict';

// Import the required models
let groupMember = rootRequire('app/models/groupMemberModels/GroupMember');
let User = rootRequire('app/models/userModels/User');
let UserGroup = rootRequire('app/models/groupModels/UserGroup');

// Import logger to log API calls
let logger = rootRequire('logs/logger');

// Define association between the tables here;
groupMember.belongsTo(User, {foreignKey:'user_id'});
groupMember.belongsTo(UserGroup, {foreignKey:'group_id'});


//Functions to interact with models/database

module.exports = {
    showAllMembers:{
        get(req, res){
            groupMember.findAndCountAll({
                where:{
                    group_id: req.params.group_id
                },
                include:[
                    {
                        model: User
                    }
                ]
            }).then(groupMembers => {
                let members = [];
                for(let key in groupMembers.rows){
                    members.push(groupMembers.rows[key].user);
                }

                const response = {status_code: 200, msg: 'Success fetching groupmembers', data : members};
                logger.log('info', 'API: /groups/:group_id/members function: showAllMember ' + JSON.stringify(response));
                res.send(response);
            }).catch(err => {
                const response = {status_code: 503, msg: 'Error fetching groupmembers', data : []};
                logger.log('error', 'API: /groups/:group_id/members function: showAllMember ' + JSON.stringify(response));
                res.send(response);
            })
        }
    },
    showAllGroups:{
        get(req, res){
            groupMember.findAndCountAll({
                where:{
                    user_id:req.params.member_id
                },
                include:[
                    {
                        model: UserGroup
                    }
                ]
            }).then(memberGroups=> {
                let groups = [];
                for(let key in memberGroups.rows){
                    groups.push(memberGroups.rows[key].user_group);
                }
                const allMembers = {
                    totalGroups:memberGroups.count,
                    groups
                };
                const response = {status_code: 200, msg: 'Success fetching groupmembers', data : allMembers};
                logger.log('info', 'API: /groups/:group_id/members/:member_id function: showAllGroups ' + JSON.stringify(response));
                res.send(response);
            }).catch(err => {
                const response = {status_code: 503, msg: 'Error fetching groupmembers', data : []};
                logger.log('error', 'API: /groups/:group_id/members/:member_id function: showAllGroups ' + JSON.stringify(response));
                res.send(response);
            })
        }
    },
    addMember:{
        post(req, res){
            let groupmembers = [];
            for(let key in req.body){
                let member = {};
                member['user_id'] = req.body[key];
                member['group_id'] = req.params.group_id;
                groupmembers.push(member);
            }
            console.log(groupmembers);
            groupMember.bulkCreate(
                groupmembers
            ).then(() =>{
                logger.log('info', 'API: /groups/:group_id/members function: addMember');
                return module.exports.showAllMembers.get(req, res);
            }).catch(err => {
                const response = {status_code: 503, msg: 'Error fetching groupmembers', data : []};
                logger.log('error', 'API: /groups/:group_id/members function: addMember ' + JSON.stringify(response));
                res.send(response);
            })
        }
    },
    editMember:{
        put(req, res){

        }
    },
    deleteMember:{
        delete(req, res){
            groupMember.destroy({
                where:{
                    user_id:req.params.member_id,
                    group_id:req.params.group_id
                }
            }).then(() =>{
                logger.log('info', 'API: /groups/:group_id/members/:member_id function: deleteMember');
                return module.exports.showAllMembers.get(req, res);
            }).catch(err => {
                const response = {status_code: 503, msg: 'Error fetching groupmembers', data : []};
                logger.log('error', 'API: /groups/:group_id/members/:member_id function: deleteMember ' + JSON.stringify(response));
                res.send(response);
            })
        }
    }
};