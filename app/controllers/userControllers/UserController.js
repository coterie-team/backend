/**
 Created by rahilvora on 07/30/17
 */

'use strict';

let User = rootRequire('app/models/userModels/User');
let GroupMember = rootRequire('app/models/groupMemberModels/GroupMember');
let UserGroup = rootRequire('app/models/groupModels/UserGroup');
let Task = rootRequire('app/models/taskModels/Task');
let TaskAssignedTo = rootRequire('app/models/taskModels/TaskAssignedTo');
let TaskQueue = rootRequire('app/models/taskModels/TaskQueue');
let checkUtil = rootRequire('utils/checkUtil');

let constants = rootRequire('utils/constants');
let db = rootRequire('config/db');
var bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {
    login: {
        get(req, res){
            res.render('login');
        },
        post(req, res){

            if (checkUtil.isEmpty(req.body)) {
                res.send({status_code: 400, msg: 'No message body found, bad request', data: {}});
                return;
            }

            if (checkUtil.isEmpty(req.body.email)) {
                res.send({
                    status_code: 400, msg: 'Bad request. One of the required param is not found. Required are ' +
                    '{email, password}', data: {}
                });
                return;
            }

            // Manual login
            if (req.body.password != null) {
                User.findOne({
                    where: {email: req.body.email}
                }).then(user => {
                    console.log(user);
                    if (user != null) {
                        // Comparing input password with hash in DB
                        bcrypt.compare(req.body.password, user.password, function (bcrypt_err, bcrypt_res) {
                            if (bcrypt_err) {
                                res.send({
                                    status_code: 400,
                                    msg: 'Something went wrong',
                                    data: {}
                                });
                            } else if (bcrypt_res) {
                                req.session.user_email = user.email;
                                req.session.user_id = user.id;
                                console.log("Session initialized for : " + req.session.user_email);
                                let response = {
                                    status_code: 200,
                                    msg: 'Login successful',
                                    data: user
                                };
                                res.send(response);
                            } else {
                                let msg = 'Entered password is not valid';
                                if (user.source == constants.getLoginType(2)) {
                                    msg = 'We have you registered using Google, please Continue with Google';
                                } else if (user.source == constants.getLoginType(3)) {
                                    msg = 'We have you registered using Facebook, please Continue with Facebook';
                                }
                                let response = {
                                    status_code: 400,
                                    msg: msg,
                                    data: null
                                };
                                res.send(response);
                            }
                        });
                    } else {
                        res.send({
                            status_code: 400,
                            msg: 'Entered email is not valid',
                            data: {}
                        });
                    }
                })
            } else {
                // This happens when using Facebook or Google for login
                User.findOne({
                    where: {email: req.body.email}
                }).then(user => {
                    console.log(user);
                    if (user != null) {
                        req.session.user_email = user.email;
                        req.session.user_id = user.id;
                        console.log("Session initialized for : " + req.session.user_email);
                        let response = {
                            status_code: 200,
                            msg: 'Login successful',
                            data: user
                        };
                        res.send(response);
                    } else {
                        let response = {
                            status_code: 400,
                            msg: 'Entered credentials are not valid',
                            data: null
                        };
                        res.send(response);
                    }
                })
            }
        }
    },
    signup: {
        get(req, res){
            let response = {
                status_code: 200,
                msg: 'user_registered'
            };
            res.send(response);
        }
    },
    checkLogin: {
        post(req, res){
            var temp;
            if (req.session.user_email == req.body.email) {
                temp = 'user logged in'
            } else {
                temp = 'user not logged in'
            }
            let response = {
                status_code: 200,
                msg: temp
            };
            res.send(response);
        }
    },
    register: {
        post(req, res){

            if(checkUtil.isEmpty(req.body)) {
                return res.send({status_code: 400, msg: 'Bad request, no request body', data:{}});
            }

            checkIfUserExists(req.body.email).then(user => {
                if (user == null) {
                    let password = req.body.password;

                    if(!checkUtil.isEmpty(password)) {
                        password = bcrypt.hashSync(req.body.password, saltRounds);
                    }

                    var source = constants.getLoginType(req.body.source);
                    User.create({
                        first_name: req.body.first_name,
                        last_name: req.body.last_name,
                        email: req.body.email,
                        password: password,
                        phone_number: req.body.phone,
                        photo_url: req.body.photo_url,
                        source: source

                    }).then(user => {
                        req.session.user_email = user.email;
                        req.session.user_id = user.id;
                        console.log("Registration successful");
                        console.log("Session initialized for : " + req.session.user_email);
                        let response = {
                            status_code: 200,
                            msg: 'User registration successful',
                            data: user
                        };
                        res.send(response);
                    }).catch(err => {
                        console.log("in error " + err);
                        let response = {
                            status_code: 500,
                            msg: 'User registration failed, please try again'
                        };
                        res.send(response);
                    });
                } else {
                    console.log("user exists");
                    var message = 'This email is already registered with us.';
                    if (user.source === constants.getLoginType(2)) {
                        message += ' Continue with Google to login'
                    }
                    if (user.source == constants.getLoginType(3)) {
                        message += ' Continue with Facebook to login'
                    }
                    let response = {
                        status_code: 409,
                        msg: message
                    };
                    res.send(response);
                }
            });
        }
    },
    verifyEmail: {
        post(req, res){
            checkIfUserExists(req.body.email).then(user => {
                if (user != null) {
                    // 1. Normal login email
                    // 2. Google login email
                    // 3. Facebook login email
                    let msg = 'An email is sent on the address to recover password';
                    let status_code = 200;
                    if (checkUtil.isNotEmpty(req.body.source) && user.source != constants.getLoginType(req.body.source)) {
                        if (user.source == constants.getLoginType(2)) {
                            status_code = 409;
                            msg = 'We have you registered using Google, please Continue with Google';
                        } else if (user.source == constants.getLoginType(3)) {
                            status_code = 409;
                            msg = 'We have you registered using Facebook, please Continue with Facebook';
                        } else if (user.source == constants.getLoginType(1)) {
                            status_code = 409;
                            msg = 'We have your email, please manually login to continue.';
                        }
                    }
                    // TODO In future write code to send email to the user with recovery link
                    let response = {
                        status_code: status_code,
                        msg: msg,
                        data: user
                    };
                    res.send(response);
                } else {
                    let response = {
                        status_code: 400,
                        msg: 'Email provided is not found in our records',
                        data: null
                    };
                    res.send(response);
                }
            });
        }
    },
    verifyUserId: {
        get(req, res){
            console.log("user i :" + req.user_id);
            checkIfUserIdExists(req.user_id).then(user => {
                if (user != null) {
                    let response = {
                        status_code: 200,
                        msg: 'user_id verified'
                    };
                    res.send(response);
                }
            });
        }
    },
    findProfileById: {
        get(req, res){
            User.findAll({
                where: {id: req.params.id},
                attributes: ['id', 'first_name', 'last_name', 'email', 'phone_number', 'photo_url']
            }).then(user => {
                if (user.length == 1) {
                    let response = {
                        status_code: 200,
                        msg: 'user profile founded',
                        data: user,
                    };
                    res.send(response)
                } else {
                    let response = {
                        status_code: 200,
                        msg: 'data not found'
                    };
                    res.send(response)
                }

            }).catch(err => {
                console.log("in error " + err);
                let response = {
                    status_code: 500,
                    msg: 'fetching profile error'
                };
                res.send(response);
            });
        }
    },
    findProfileByName: {
        get(req, res){
            User.findAll({
                where: {
                    first_name: req.params.first_name,
                    last_name: req.params.last_name
                },
                attributes: ['id', 'first_name', 'last_name', 'email', 'phone_number', 'photo_url']
            }).then(user => {
                if (user.length > 0) {
                    let response = {
                        status_code: 200,
                        msg: 'user profile founded',
                        data: user,
                    };
                    res.send(response)
                } else {
                    let response = {
                        status_code: 200,
                        msg: 'data not found'
                    };
                    res.send(response)
                }

            }).catch(err => {
                console.log("in error " + err);
                let response = {
                    status_code: 500,
                    msg: 'fetching profile error'
                };
                res.send(response);
            });
        }
    },
    findProfileByEmail: {
        get(req, res){
            User.findAll({
                where: {email: req.params.email},
                attributes: ['id', 'first_name', 'last_name', 'email', 'phone_number', 'photo_url']
            }).then(user => {
                if (user.length == 1) {
                    let response = {
                        status_code: 200,
                        msg: 'user profile founded',
                        data: user,
                    };
                    res.send(response)
                } else {
                    let response = {
                        status_code: 200,
                        msg: 'data not found'
                    };
                    res.send(response)
                }

            }).catch(err => {
                console.log("in error " + err);
                let response = {
                    status_code: 500,
                    msg: 'fetching profile error'
                };
                res.send(response);
            });
        }
    },
    searchUser: {
        get(req, res) {
            if (checkUtil.isEmpty(req.params.term) || req.params.term.length < 3) {
                let response = {
                    status_code: 400,
                    msg: 'Either search term is not present or less than 2',
                    data: {}
                };
                res.send(response);
                return;
            }
            let search_term = req.params.term + '%';
            db.query('SELECT email, id, first_name, last_name, phone_number, photo_url FROM user WHERE first_name LIKE :search_name ',
                {replacements: {search_name: search_term}, type: db.QueryTypes.SELECT}
            ).then(users => {
                console.log('Users found for the search term ' + users);
                let response = {
                    status_code: 200,
                    msg: 'Found users for search term',
                    data: users
                };
                res.send(response);
            });
        }
    },
    updateUserProfile: {
        put(req, res){
            let user_id = req.params.user_id;
            let body = req.body;

            if (checkUtil.isEmpty(user_id)) {
                console.log('Either user_id is empty returning error');
                var response = {status_code: 400, msg: 'Bad request', data: {}};
                res.send(response);
                return;
            }

            if (checkUtil.isEmpty(body)) {
                res.send({status_code: 400, msg: 'No message body found, bad request', data: {}});
                return;
            }

            if (checkUtil.isEmpty(body.first_name) || checkUtil.isEmpty(body.last_name) || checkUtil.isEmpty(body.password)
                || checkUtil.isEmpty(body.phone_number)) {
                res.send({
                    status_code: 400,
                    msg: 'Bad request. first_name, last_name, password, phone_number are required',
                    data: {}
                });
                return;
            }

            let user = {};
            user.first_name = body.first_name;
            user.last_name = body.last_name;
            user.password = bcrypt.hashSync(body.password, saltRounds);
            user.phone_number = body.phone_number;

            if (checkUtil.isEmptyObject(user)) {
                res.send({status_code: 400, msg: 'Bad request. No parameter to update', data: {}});
                return;
            }

            User.update(user,
                {
                    where: {id: user_id}
                }
            ).then(user => {
                if (user == 1) {
                    User.findOne({where: {id: user_id}}).then(updatedUser => {
                        let response = {status_code: 200, msg: 'User profile updated', data: updatedUser};
                        res.send(response)
                    });
                } else {
                    let response = {
                        status_code: 200, msg: "Failed to updated user profile, try again", data: {}
                    };
                    res.send(response)
                }
            }).catch(err => {
                console.log("Error updating user profile " + err);
                let response = {status_code: 500, msg: 'Something went wrong updating user profile', data: {}};
                res.send(response);
            });
        },

        delete(req, res){
            User.destroy({
                where: {
                    id: req.params.id
                }
            }).then(user => {
                if (user == 1) {
                    let response = {
                        status_code: 200,
                        msg: 'successfully deleted',
                    };
                    res.send(response)
                } else {
                    let response = {
                        status_code: 200,
                        msg: 'data not found',
                    };
                    res.send(response)
                }
            }).catch(err => {
                console.log("in error " + err);
                let response = {
                    status_code: 500,
                    msg: 'user delete error'
                };
                res.send(response);
            });
        }
    },

    updatePassword: {
        put(req, res){
            User.update({password: req.body.password},
                {
                    where: {id: req.params.id}
                }
            ).then(user => {
                if (user == 1) {
                    let response = {
                        status_code: 200,
                        msg: 'password updated successfully'
                    };
                    res.send(response)
                } else {
                    let response = {
                        status_code: 200,
                        msg: "password not updated sucessfully"
                    };
                    res.send(response)
                }
            }).catch(err => {
                console.log("in error " + err);
                let response = {
                    status_code: 500,
                    msg: 'password update error'
                };
                res.send(response);
            });
        },
    },

    tasks: {
        get(req, res){
            //Return all the tasks
            let user_id = req.params.user_id;
            console.log('Returning tasks for user id ' + user_id);
            if (checkUtil.isEmpty(user_id)) {
                console.log('Either task id or group id is empty returning error');
                var response = {status_code: 400, msg: 'Bad request', data: {}};
                res.send(response);

                return;
            }
            //	include: [{model: User, required: true}] ,
            TaskQueue.belongsTo(Task, {foreignKey: 'task_id'});
            TaskQueue.belongsTo(UserGroup, {foreignKey: 'group_id'});
            TaskQueue.belongsTo(TaskAssignedTo, {foreignKey: 'task_id', targetKey: 'task_id'});
            TaskAssignedTo.belongsTo(User, {foreignKey: 'assigned_user_id'});
            TaskQueue.findAll({
                include: [{model: Task, required: true}, {model: UserGroup, required: true},
                    {model: TaskAssignedTo, include: [{model: User, required: true}], required: true}],
                where: {
                    user_id: user_id
                },
                limit: 25,
            }).then(allTasks => {
                let promiseObj = [];
                allTasks.map(taskAssignedTo => {
                    let prom = new Promise(function (resolve, reject) {
                        db.query("SELECT `user`.`id`, `user`.`first_name`, `user`.`last_name`, `user`.`email`, `user`.`phone_number`, `user`.`photo_url`, " +
                            "`task_queue`.`task_id`, `task_queue`.`user_seq` FROM `user` JOIN `task_queue` ON `user`.`id` = `task_queue`.`user_id` " +
                            "WHERE `task_queue`.`task_id` = " + taskAssignedTo.task.id, {type: db.QueryTypes.SELECT})
                            .then(users => {

                                // let response = {status_code: 200, data: users, msg: 'User sequence for all user\'s task'};
                                // res.send(response);
                                console.log(JSON.stringify(users));
                                let obj = {
                                    task_id: taskAssignedTo.task.id,
                                    task_name: taskAssignedTo.task.name,
                                    task_frequency: taskAssignedTo.task.frequency,
                                    task_start_date: taskAssignedTo.task.start_date,
                                    task_type: taskAssignedTo.task.type,
                                    assign_type: taskAssignedTo.task.assign_type,
                                    user_involved_id: taskAssignedTo.user_id,
                                    group_id: taskAssignedTo.task.group_id,
                                    group_name: taskAssignedTo.user_group.name,
                                    assigned_user_id: taskAssignedTo.task_assigned_to.assigned_user_id,
                                    assigned_user_name: taskAssignedTo.task_assigned_to.user.first_name,
                                    users: users
                                };
                                console.log('Pushing objects');
                                resolve(obj);
                            });
                    });
                    promiseObj.push(prom);
                });

                Promise.all(promiseObj).then(values => {
                    var response = {status_code: 200, msg: 'Success fetching tasks', data: values};
                    res.send(response);
                });
            });
        }
    },

    groups: {

        get(req, res){
            let user_id = req.params.user_id;
            console.log("Fetching groups for user_id: " + user_id);
            GroupMember.findAndCountAll({
                include: [UserGroup],
                where: {user_id: user_id},
                attributes: ['user_id']
            }).then(groupsOfUser => {
                console.log("User group " + JSON.stringify(groupsOfUser));
                let group_data = [];
                for (let key in groupsOfUser.rows) {
                    group_data.push(groupsOfUser.rows[key].user_group);
                }

                let response = {
                    status_code: 200,
                    data: group_data,
                    msg: 'User group data'
                };
                res.send(response);
            })
        },
    }
};

function checkIfUserExists(email) {
    console.log("checkIfUserExists");
    return User.findOne({
        where: {email: email}
    })
}

function checkIfUserPhoneExists(phone) {
    console.log("checkIfUserPhoneExists");
    return User.findOne({
        where: {phone_number: phone}
    })
}

function checkIfUserIdExists(id) {
    console.log("checkIfUserExists");
    return User.findOne({
        where: {id: id}
    })
}