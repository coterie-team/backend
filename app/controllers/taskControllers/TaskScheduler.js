/**
 * Created by rishi on 11/1/17.
 */

'use strict';

let Task = rootRequire('app/models/taskModels/Task');
let TaskAssignedTo = rootRequire('app/models/taskModels/TaskAssignedTo');
let TaskQueue = rootRequire('app/models/taskModels/TaskQueue');
let NotificationController = rootRequire('app/controllers/notificationController/NotificationController');

let schedule = require('node-schedule');
let constants = rootRequire('utils/constants');

module.exports = {
    initAllSchedules(req){
        console.log('**** Start initializing all the schedules in the DB ****');
        Task.findAll().then(all_tasks => {
            all_tasks.map(task => {
                if(task.schedule_id != '-1') {
                    let schedule_rule = getScheduleRule(task, false);
                    let old_job_with_id = schedule.scheduledJobs[task.schedule_id];
                    schedule.cancelJob(old_job_with_id);

                    let old_notification_schedule_id = schedule.scheduledJobs[getNotificationScheduleId(task)];
                    schedule.cancelJob(old_notification_schedule_id);

                    scheduleTasks(req, task, schedule_rule);
                    scheduleNotification(task);
                    console.log('Job scheduled for task '+ task.id);
                }
            });
            console.log('**** End initializing all the schedules in the DB ****');
        });
    },

    updateTaskSchedule(req, updatedTask) {
        console.log('Updating the schedule job for '+ updatedTask.id);
        let old_job_with_id = schedule.scheduledJobs[updatedTask.schedule_id];
        schedule.cancelJob(old_job_with_id);

        let old_notification_schedule_id = schedule.scheduledJobs[getNotificationScheduleId(updatedTask)];
        schedule.cancelJob(old_notification_schedule_id);
        
        scheduleTasks(req, updatedTask, getScheduleRule(updatedTask, false));
        scheduleNotification(updatedTask);
    },

    deleteTaskSchedule(task) {
        console.log('Deleting the schedule job for '+ task.id);
        let old_job_with_id = schedule.scheduledJobs[task.schedule_id];
        schedule.cancelJob(old_job_with_id);
    }
};

function scheduleTasks(req, task, scheduleRule) {
    var job = schedule.scheduleJob(task.schedule_id, scheduleRule, function (task) {
        // 1. Find current sequence index and user id from task_assigned_to for task.id and task.group_id (unique)
        // 2. Increment the sequence index
        // 3. Get max sequence index from task_queue for task.id (unique)
        // 4. If curr is greater than max reset it to 1, else use it.
        // 5. Update the task_assigned_to table

        TaskAssignedTo.findOne({where: {task_id: task.id, group_id: task.group_id}}).then((taskAssignedTo) => {
            TaskQueue.max('user_seq', {where: {task_id: task.id}}).then(max_user_seq => {
                let current_seq = taskAssignedTo.assign_seq;
                let old_user_id = taskAssignedTo.assigned_user_id;
                current_seq++;
                if (current_seq > max_user_seq) {
                    current_seq = 0;
                }
                TaskQueue.findOne({where: {task_id: task.id, user_seq: current_seq}}).then(user_queue => {
                    TaskAssignedTo.update({assigned_user_id: user_queue.user_id, assign_seq: current_seq}, {
                        where: {task_id: task.id, group_id: task.group_id}
                    }).then(()=> {
                        console.log(task.name + ' task assignment changed from old user id ' + old_user_id + ' to new user id ' + user_queue.user_id);
                        if (req.app.get('eventEmitter') !== undefined) {
                            req.app.get('eventEmitter').emitTaskAssignedUpdate('update', user_queue.user_id);
                        }
                    })
                });
            });
        });
    }.bind(null, task));

    console.log('Setting schedule for notifications, 9 pm a day before');
    let notification_rule = getScheduleRule(task, true);

}

function scheduleNotification(task) {
    console.log('Setting schedule for notifications, 9 pm a day before');
    let notification_rule = getScheduleRule(task, true);
    let notification_schedule_id = getNotificationScheduleId(task);

    var job = schedule.scheduleJob(notification_schedule_id, notification_rule, function (task) {
        // 1. Find current sequence index and user id from task_assigned_to for task.id and task.group_id (unique)
        // 2. Increment the sequence index
        // 3. Get max sequence index from task_queue for task.id (unique)
        // 4. If curr is greater than max reset it to 1, else use it.
        // 5. Send the notification

        TaskAssignedTo.findOne({where: {task_id: task.id, group_id: task.group_id}}).then((taskAssignedTo) => {
            TaskQueue.max('user_seq', {where: {task_id: task.id}}).then(max_user_seq => {
                let current_seq = taskAssignedTo.assign_seq;
                let old_user_id = taskAssignedTo.assigned_user_id;
                current_seq++;
                if (current_seq > max_user_seq) {
                    current_seq = 0;
                }
                TaskQueue.findOne({where: {task_id: task.id, user_seq: current_seq}}).then(user_queue => {
                    console.log('Sending notification to the user');
                    // Add firebase-admin code to send notifications
                    NotificationController.sendTaskReminderNotification(user_queue.user_id, user_queue.task_id)
                        .then(response => {
                            if(response) {
                                console.log('Notification sent!!!')
                            }else {
                                console.log('Failed to sent notification!!!');
                            }
                        })
                        .catch(err => {
                            console.log('Something went wrong sending notification '+ err);
                        });
                });
            });
        });
    }.bind(null, task));
}

function getScheduleRule(newTask, is_notification) {
    let rule = null;
    let frequency = newTask.frequency;
    let start_date = newTask.start_date;

    if(is_notification) {
        let previous_day = newTask.start_date;
        previous_day.setDate(previous_day.getDate() - 1);
        start_date = previous_day;
    }

    if (frequency == constants.getFrequency(1)) {
        rule = getDailyRule(start_date.getDay(), is_notification);
    } else if (frequency == constants.getFrequency(2)) {
        rule = getWeeklyRule(start_date.getDay(), is_notification);
    } else if (frequency == constants.getFrequency(3)) {
        rule = getBiWeeklyRule(start_date.getDate(), is_notification);
    } else if (frequency == constants.getFrequency(4)) {
        rule = getMonthlyRule(start_date.getDate(), is_notification);
    } else if (frequency == constants.getFrequency(5)) {
        rule = getOnceRule(start_date.getMinutes(), start_date.getHours(), start_date.getDate(), start_date.getMonth(), start_date.getFullYear());
    } else if (frequency == constants.getFrequency(100)) {
        rule = getTestRule();
    }

    return rule;
}

function getDailyRule(day_of_week, is_notification) {
    var rule = new schedule.RecurrenceRule();
    rule.dayOfWeek = [day_of_week, new schedule.Range(0, 6)];
    rule.hour = is_notification ? 20 : 8;
    rule.minute = 0;

    return rule;
}

function getWeeklyRule(day_of_week, is_notification) {
    var rule = new schedule.RecurrenceRule();
    rule.dayOfWeek = day_of_week;
    rule.hour = is_notification ? 20 : 8;
    rule.minute = 0;
    rule.month = new schedule.Range(0, 11);

    return rule;
}

function getBiWeeklyRule(start_date, is_notification) {
    var rule = new schedule.RecurrenceRule();
    rule.hour = is_notification ? 20 : 8;
    rule.minute = 0;
    // start date is from 1 to 31
    rule.date = [start_date, new schedule.Range(1, 31, 15)];

    return rule;
}

function getMonthlyRule(start_date, is_notification) {
    var rule = new schedule.RecurrenceRule();
    rule.month = new schedule.Range(0, 11);
    rule.hour = is_notification ? 20 : 8;
    rule.minute = 0;
    // start date is from 1 to 31
    rule.date = start_date;

    return rule;
}

function getOnceRule(minute, hour, date, month, year) {
    var rule = new schedule.RecurrenceRule();
    rule.hour = hour;
    rule.minute = minute;
    rule.date = date;
    rule.month = month;
    rule.year = year;

    return rule;
}

function getTestRule() {
    var rule = new schedule.RecurrenceRule();
    rule.second = 5;

    return rule;
}

function getNotificationScheduleId(task) {
    return task.name + '_' + task.id + '_' + task.group_id + '_notification';
}

module.exports.getScheduleRule = getScheduleRule;
module.exports.scheduleTasks = scheduleTasks;
module.exports.scheduleNotification = scheduleNotification;