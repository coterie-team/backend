/**
 Created by rahilvora on 07/30/17
 */

'use strict';
let User = rootRequire('app/models/userModels/User');
let UserGroup = rootRequire('app/models/groupModels/UserGroup');
let Task = rootRequire('app/models/taskModels/Task');
let TaskAssignedTo = rootRequire('app/models/taskModels/TaskAssignedTo');
let TaskQueue = rootRequire('app/models/taskModels/TaskQueue');

let checkUtil = rootRequire('utils/checkUtil');
let constants = rootRequire('utils/constants');
let taskScheduler = rootRequire('app/controllers/taskControllers/TaskScheduler');
let NotificationController = rootRequire('app/controllers/notificationController/NotificationController');

let db = rootRequire('config/db');

module.exports = {
    showTasks: {
        get(req, res){
            //Return all the tasks
            let group_id = req.group_id;
            console.log('Returning tasks for group id ' + group_id);
            if (checkUtil.isEmpty(group_id)) {
                console.log('Group Id is empty returning error');
                var response = {status_code: 400, msg: 'Bad request', data: {}};
                res.send(response);
                return;
            }
            TaskAssignedTo.belongsTo(UserGroup, {foreignKey: 'group_id', targetKey: 'id'});
            TaskAssignedTo.belongsTo(Task, {foreignKey: 'task_id', targetKey: 'id'});
            TaskAssignedTo.belongsTo(User, {foreignKey: 'assigned_user_id'});
            TaskAssignedTo.findAll({
                include: [{model: Task, required: true}, {model: UserGroup, required: true},
                    {model: User, required: true}],
                where: {
                    group_id: group_id
                },
                limit: 25,
            }).then(allTasks => {
                let promiseObj = [];
                allTasks.map(taskAssignedTo => {
                    let prom = new Promise(function (resolve, reject) {
                        db.query("SELECT `user`.`id`, `user`.`first_name`, `user`.`last_name`, `user`.`email`, `user`.`phone_number`, `user`.`photo_url`, " +
                            "`task_queue`.`task_id`, `task_queue`.`user_seq` FROM `user` JOIN `task_queue` ON `user`.`id` = `task_queue`.`user_id` " +
                            "WHERE `task_queue`.`task_id` = "+ taskAssignedTo.task.id, {type: db.QueryTypes.SELECT})
                            .then(users => {

                                // let response = {status_code: 200, data: users, msg: 'User sequence for all user\'s task'};
                                // res.send(response);
                                console.log(JSON.stringify(users));
                                let obj = {
                                    task_id: taskAssignedTo.task.id,
                                    task_name: taskAssignedTo.task.name,
                                    task_frequency: taskAssignedTo.task.frequency,
                                    task_start_date: taskAssignedTo.task.start_date,
                                    task_type: taskAssignedTo.task.type,
                                    assign_type: taskAssignedTo.task.assign_type,
                                    user_involved_id: taskAssignedTo.user_id,
                                    group_id: taskAssignedTo.task.group_id,
                                    group_name: taskAssignedTo.user_group.name,
                                    assigned_user_id: taskAssignedTo.user_id,
                                    assigned_user_name: taskAssignedTo.user.first_name,
                                    users: users
                                };
                                console.log('Pushing objects');
                                resolve(obj);
                            });
                    });
                    promiseObj.push(prom);
                });

                Promise.all(promiseObj).then(values => {
                    var response = {status_code: 200, msg: 'Success fetching tasks for group', data : values};
                    res.send(response);
                });
            });
        },
    },

    editTask: {
        put(req, res){
            console.log('Updating the task for group id ' + req.group_id);
            console.log('Task Id ' + req.params.task_id);
            let task_id = req.params.task_id;
            let group_id = req.group_id;
            let body = req.body;

            if (checkUtil.isEmpty(task_id) || checkUtil.isEmpty(group_id)) {
                console.log('Either task id or group id is empty returning error');
                var response = {status_code: 400, msg: 'Bad request', data: {}};
                res.send(response);
                return;
            }

            if (checkUtil.isEmpty(body)) {
                res.send({status_code: 400, msg: 'No message body found, bad request', data: {}});
                return;
            }

            if (checkUtil.isEmpty(body.name) || checkUtil.isEmpty(body.frequency) || checkUtil.isEmpty(body.type)
                || checkUtil.isEmpty(group_id) || checkUtil.isEmpty(body.members) || checkUtil.isEmpty(body.start_date)
                || checkUtil.isEmpty(body.assign_type)) {
                res.send({
                    status_code: 400, msg: 'Bad request. One of the required param is not found. Required are ' +
                    '{name, frequency, type, group_id, members[], start_date, assign_type}', data: {}
                });
                return;
            }

            let task = {};
            let fields = [];
            let frequency_update = false;
            let members_update = false;
            let assign_type_update = false;
            let start_date_update = false;


            let currentUserIds = [];
            let taskQueueIds = [];
            let db_task = null;

            TaskQueue.belongsTo(Task, {foreignKey: 'task_id', targetKey: 'id'});
            TaskQueue.findAll({
                include: [{model: Task, required: true}],
                where: {task_id: task_id, group_id: group_id}
            }).then(all_task_queues => {
                all_task_queues.map(task_queue => {
                    currentUserIds.push(task_queue.user_id);
                    taskQueueIds.push(task_queue.id);
                    db_task = task_queue.task;
                    console.log('Done DB task '+ db_task);
                });

                if(checkUtil.isEmpty(db_task)) {
                    var response = {status_code: 404, msg: 'Task for task_id: ' + task_id + ' not found', data: {}};
                    res.send(response);
                    return;
                }

                if (db_task.name != body.name) {
                    fields.push('name');
                    task.name = body.name;
                }

                if (db_task.frequency != constants.getFrequency(body.frequency)) {
                    fields.push('frequency');
                    task.frequency = constants.getFrequency(body.frequency);
                    frequency_update = true;
                }

                if (db_task.type != constants.getTaskType(body.type)) {
                    fields.push('type');
                    task.type = constants.getTaskType(body.type);
                }

                if (db_task.start_date != body.start_date) {
                    fields.push('start_date');
                    task.start_date = new Date(body.start_date);
                    start_date_update = true;
                }

                if (db_task.assign_type != constants.getAssignType(body.assign_type)) {
                    fields.push('assign_type');
                    task.assign_type = constants.getAssignType(body.assign_type);
                    assign_type_update = true;
                }


                for (let i = 0; i < body.members.length; i++) {
                    if (currentUserIds.indexOf(parseInt(body.members[i])) == -1) {
                        members_update = true;
                        break;
                    }
                }

                console.log('Task to update ' + task);
                console.log('Updates are ' + members_update + ' ' + frequency_update + ' ' + assign_type_update + ' ' + start_date_update);

                let inputUserIds = members_update ? body.members : currentUserIds;
                let assign_type = assign_type_update ? constants.getAssignType(body.assign_type) : db_task.assign_type;

                if (db_task != null) {
                    if (inputUserIds.length == currentUserIds.length) {
                        // update the existing tables
                        applyAssignTypeAndUpdate(assign_type, db_task.id, inputUserIds, taskQueueIds);

                    } else {
                        // Delete all current users and recreate based on input users
                        deleteCurrentMembers(db_task, currentUserIds, taskQueueIds)
                            .then(result => {
                                if (result) {
                                    addNewMembers(db_task, inputUserIds)
                                        .then(result => {
                                            if (result) {
                                                console.log('Task members updated');
                                                applyAssignTypeAndUpdate(assign_type, db_task.id, inputUserIds, taskQueueIds);
                                            } else {
                                                console.log('Failed to add new task members');
                                            }
                                        })
                                } else {
                                    console.log('Failed to delete existing task members');
                                }
                            });
                    }
                } else {
                    console.log('Why task is null here!!!');
                }

                Task.update(task,
                    {
                        where: {id: task_id, group_id: group_id}
                    })
                    .then(task => {
                        Task.findOne({where: {id: task_id}}).then(updatedTask => {
                            if (members_update || frequency_update || assign_type_update || start_date_update) {
                                taskScheduler.updateTaskSchedule(req, updatedTask);
                            }
                            var response = {status_code: 200, msg: 'Success updating task', data: updatedTask};
                            res.send(response);
                        });
                    });
            });
        }
    },

    deleteTask: {
        delete(req, res){
            console.log('Deleting the task id ' + req.params.task_id);
            let task_id = req.params.task_id;
            let group_id = req.group_id;

            if (checkUtil.isEmpty(task_id) || checkUtil.isEmpty(group_id)) {
                console.log('Either task id or group id is empty returning error');
                var response = {
                    status_code: 400,
                    msg: 'Bad request, either task_id or group_id is not in URL param',
                    data: {}
                };
                res.send(response);
                return;
            }

            TaskQueue
                .destroy(
                    {
                        where: {task_id: task_id, group_id: group_id}
                    })
                .then(deletedRows => {
                    TaskAssignedTo
                        .destroy(
                            {
                                where: {task_id: task_id, group_id: group_id}
                            })
                        .then(deletedTaskAssignedTo => {
                            Task.findOne({where: {id: task_id}}).then(toDeleteTask => {
                                // Stop the schedule if any
                                if(toDeleteTask.schedule_id != '-1') {
                                    taskScheduler.deleteTaskSchedule(toDeleteTask);
                                }
                                Task.destroy(
                                    {
                                        where: {id: task_id, group_id: group_id}
                                    })
                                    .then(deletedTask => {
                                        console.log('Number of tasks deleted ' + deletedTask);
                                        var response = {status_code: 200, msg: 'Success deleting task', data: deletedTask};
                                        res.send(response);
                                    });
                            });
                        });
                }).catch(error => {
                var response = {status_code: 500, msg: 'Failed to delete task, try again', data: []};
                res.send(response);
            });
        }
    },

    usersInvolvedInTask: {
        get(req, res) {
            console.log('Getting users for task Id ' + req.query.task_id + ' and group id ' + req.group_id);
            let task_id = req.params.task_id;
            let group_id = req.group_id;

            if (checkUtil.isEmpty(task_id) || checkUtil.isEmpty(group_id)) {
                console.log('Either task id or group id is empty returning error');
                var response = {status_code: 400, msg: 'Bad request', data: {}};
                res.send(response);
                return;
            }
            TaskQueue.belongsTo(User, {foreignKey: 'user_id'});
            TaskQueue.findAll({
                include: [{model: User, required: true}],
                where: {
                    task_id: task_id,
                    group_id: group_id
                },
                order: ['user_seq'],
                limit: 25,
            }).then(allUsers => {
                let userData = [];
                for (var i = 0; i < allUsers.length; i++) {
                    var tempObj = {
                        id: allUsers[i].user.id,
                        first_name: allUsers[i].user.first_name,
                        last_name: allUsers[i].user.last_name,
                        email: allUsers[i].user.email,
                        phone: allUsers[i].user.phone_number,
                        photo_url: allUsers[i].user.photo_url,
                        group_id: allUsers[i].group_id,
                        user_seq: allUsers[i].user_seq
                    };
                    userData.push(tempObj);
                }
                var response = {status_code: 200, msg: 'Success fetching tasks', data: userData};
                console.log('Response with users for task ' + JSON.stringify(response));
                res.send(response);
            });
        }
    },

    addTaskForGroup: {
        post(req, res){
            let body = req.body;
            let group_id = req.group_id;
            if (checkUtil.isEmpty(body)) {
                res.send({status_code: 400, msg: 'No message body found, bad request', data: {}});
                return;
            }

            if (checkUtil.isEmpty(body.name) || checkUtil.isEmpty(body.frequency) || checkUtil.isEmpty(body.type)
                || checkUtil.isEmpty(group_id) || checkUtil.isEmpty(body.members) || checkUtil.isEmpty(body.start_date)
                || checkUtil.isEmpty(body.assign_type) || checkUtil.isEmpty(body.added_by)) {
                res.send({
                    status_code: 400, msg: 'Bad request. One of the required param is not found. Required are ' +
                    '{name, frequency, type, group_id, members[], start_date, assign_type, added_by}', data: {}
                });
                return;
            }

            // FIXME Right now AWS EC2 timezone changed to LA.
            // But needs fix in long term
            let start_date = new Date(body.start_date);

            // Add the new task
            let task = {
                name: body.name,
                frequency: constants.getFrequency(body.frequency),
                type: constants.getTaskType(body.type),
                group_id: group_id,
                start_date: start_date,
                assign_type: constants.getAssignType(body.assign_type),
                added_by: body.added_by
            };
            let userIdArray = body.members;

            if (task.assign_type == constants.getAssignType(1)) {
                getAlphabeticalUserIds(userIdArray).then(sortedUsers => {
                    createNewTask(req, res, task, sortedUsers);
                }).catch(error => {
                    console.log('Something went wrong, no sorted users');
                });
            } else {
                createNewTask(req, res, task, userIdArray);
            }
        }
    },
};

function getAlphabeticalUserIds(userIdArray) {
    return new Promise(
        function (resolve, reject) {
            User.findAll({
                attributes: ['id'],
                where: {
                    id: userIdArray
                },
                order: ['first_name'],
            }).then(sortedUsers => {
                let alphaUserIds = [];
                for (let i = 0; i < sortedUsers.length; i++) {
                    alphaUserIds.push(sortedUsers[i].id);
                }

                if (alphaUserIds.length == 0) {
                    console.log('Something went wrong, no sorted users');
                }
                resolve(alphaUserIds);
            }).catch(error => {
                console.log('Something went wrong, no sorted users');
                reject(error);
            });
        });
}

function getRandomUsers(userIdArray) {
    console.log('Input array ' + userIdArray.toString());
    let i = 0, j = 0, temp = null;
    for (i = userIdArray.length - 1; i > 0; i -= 1) {
        j = Math.floor(Math.random() * (i + 1));
        temp = userIdArray[i];
        userIdArray[i] = userIdArray[j];
        userIdArray[j] = temp;
    }

    console.log('Returning random array ' + userIdArray.toString());
    return userIdArray;
}

function onNewTaskCreated(req, res, task, userIdArray) {
    let schedule_rule = taskScheduler.getScheduleRule(task, false);
    let due_date = schedule_rule.nextInvocationDate(schedule_rule.nextInvocationDate(task.start_date));
    let today = new Date();
    let assign_seq = -1;

    if (today === task.start_date) {
        assign_seq = 0;
    }

    // Add new entry to TaskAssignedTo table
    TaskAssignedTo.create({
        group_id: task.group_id,
        assigned_user_id: userIdArray[0],
        task_id: task.id,
        assigned_date: task.start_date,
        due_date: due_date,
        assign_seq: assign_seq
    }).then(()=> {
        let user_seq = -1;
        for (var member in userIdArray) {
            if (userIdArray.hasOwnProperty(member)) {
                user_seq++;
                console.log("Assigning user id " + userIdArray[member] + ' sequence of ' + user_seq);

                // Add entry to TaskQueue
                TaskQueue.create({
                    group_id: task.group_id,
                    user_id: userIdArray[member],
                    task_id: task.id,
                    user_seq: user_seq
                });
            }
        }

        // Update the schedule_id in Task table
        Task.update({
            schedule_id: task.name + '_' + task.id + '_' + task.group_id
        }, {
            where: {id: task.id}
        }).then(() => {
            Task.findOne({where: {id: task.id}}).then(updatedTask => {
                taskScheduler.scheduleTasks(req, updatedTask, schedule_rule);
                taskScheduler.scheduleNotification(task);
                var response = {status_code: 200, msg: 'Success adding new task', data: updatedTask};

                // Sending notification to each user about new task
                NotificationController.sendTaskCreateNotification(userIdArray, task);

                res.send(response);
            });
        });
    });
}

function createNewTask(req, res, task, userIdArray) {
    Task.create(task).then(newTask => {
        onNewTaskCreated(req, res, newTask, userIdArray);
    });
}

function applyAssignTypeAndUpdate(assign_type, task_id, userIds, taskQueueIds) {
    if (assign_type == constants.getAssignType(1)) {
        getAlphabeticalUserIds(userIds).then(sortedUsers => {
            updateTaskQueueAndAssignedTo(task_id, sortedUsers, taskQueueIds);
        });
    } else {
        updateTaskQueueAndAssignedTo(task_id, getRandomUsers(userIds), taskQueueIds);
    }
}

function updateTaskQueueAndAssignedTo(task_id, userIdArray, taskQueueIdArray) {
    Task.findOne(
        {
            where: {id: task_id}
        }).then(task => {
            for (let i = 0; i < userIdArray.length; i++) {
                TaskQueue.update(
                    {
                        user_id: userIdArray[i], user_seq: i
                    },
                    {
                        where: {task_id: task.id, group_id: task.group_id, id: taskQueueIdArray[i]}
                    })
                    .then(() => {
                        console.log('TaskQueue updated for task ' + task.id + ' and queue Id ' + taskQueueIdArray[i]);
                    });
            }

            let assign_seq = -1;
            if (task.start_date === new Date()) {
                assign_seq = 0;
            }

            TaskAssignedTo.update(
                {
                    assigned_user_id: userIdArray[0], assign_seq: assign_seq
                },
                {
                    where: {task_id: task.id, group_id: task.group_id}
                })
                .then(() => {
                    console.log('Task assigned to updated for task ' + task.id + ' with user_id ' + userIdArray[0]);
                });
        });
}

function addNewMembers(task, inputUserIds) {
    return new Promise(
        function (resolve, reject) {
            for (let i = 0; i < inputUserIds.length; i++) {
                TaskQueue.create({
                    group_id: task.group_id,
                    user_id: inputUserIds[i],
                    task_id: task.id,
                    user_seq: i
                }).then(affectedRows => {
                    console.log('User ' + inputUserIds[i] + ' added to task ' + task.id);
                }).catch(error => {
                    reject(false);
                });
            }
            resolve(inputUserIds);
        }
    );

}

function deleteCurrentMembers(task, currentUserIds, taskQueueIds) {
    return new Promise(
        function (resolve, reject) {
            for (let i = 0; i < currentUserIds.length; i++) {
                TaskQueue.destroy({
                    where: {id: taskQueueIds[i], task_id: task.id, group_id: task.group_id}
                }).then(affectedRows => {
                    console.log('User ' + currentUserIds[i] + ' removed from task ' + task.id);
                }).catch(error => {
                    reject(false);
                });
            }
            resolve(true);
        }
    );
}