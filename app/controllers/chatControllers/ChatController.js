/**
	Created by rahilvora on 07/30/17
*/

'use strict';

let chat = rootRequire('app/models/chatModels/Chat');
let mysqlEventEmitter = rootRequire('./utils/mysqlEventEmitter');
let User = rootRequire('app/models/userModels/User');
let UserGroup = rootRequire('app/models/groupModels/UserGroup');
let checkUtil = rootRequire('utils/checkUtil');

let express = require('express');
let app     = express();
let http    = require('http');
let server  = http.createServer(app);
let app_config = rootRequire('config/app_config');

// let server = rootRequire('./server').server;
// let io = rootRequire('./server').io;
let io = require('socket.io')(server);

let mongoose = require('mongoose');
let chatSchema = mongoose.Schema({
	group_id: Number,
	user_id: Number,
	first_name: String,
	message: String,
	created: {type: Date, default: Date.now()}
});
let ChatMessageModel = mongoose.model('ChatMessage', chatSchema);

let numUsers = 0;
let chatSpaceName = '/';
// app.set('chat_socket', io);

module.exports = {
	setChatSpace(user_group) {
		chatSpaceName = '/' + user_group.chat_space;
		console.log('Chat space name added '+ chatSpaceName);

		initChatSpace();
	}
};

function initChatSpace() {
    let chatSpace = io.of(chatSpaceName);
    console.log('Chat space for group created with '+ chatSpace.name);
    if(chatSpace.name == '/') {
        console.log('Not a valid chat space');
        return;
    }

    chatSpace.use((socket, next) => {
        let group_id = socket.handshake.query.group_id;
        // UserGroup.findOne({
        // 	where: {id: group_id}
        // }).then(db_user_group => {
        // 	if(checkUtil.isEmpty(db_user_group)) {
        // 		console.log('No group found with group_id '+ group_id);
        // 		return next(new Error('No group found'));
        // 	}
        // 	return next();
        // });
        return next();
    });

    chatSpace.on('connection', function (socket) {
        console.log('**Chat Connected**');
        let group_id = socket.handshake.query.group_id;
        var addedUser = false;

        // Fetch old messages for the group
        var query = ChatMessageModel.find({group_id: group_id});
        query.limit(8).exec(function(err, data) {
            if(err) {
                socket.emit('error_load_old_msgs');
            }
            socket.emit('load_old_msgs', data);
            console.log(data);
        });

        // when the client emits 'new message', this listens and executes
        socket.on('new_msg', function (msg) {
            // we tell the client to execute 'new message'
            console.log('New message sent by '+ socket.first_name);
            var newMsg = new ChatMessageModel(
                {
                    group_id: group_id,
                    user_id: socket.user_id,
                    first_name: socket.first_name,
                    message: msg
                });
            newMsg.save(function(err) {
                if(err) {
                    socket.emit('error_saving_msg');
                }
                socket.broadcast.emit('new_msg', {
                    user_id: socket.user_id,
                    first_name: socket.first_name,
                    message: msg
                });
            });
        });

        // when the client emits 'add user', this listens and executes
        socket.on('add_user', function (data) {
            console.log('Adding new user '+ data.first_name + " " + addedUser);
            if (addedUser) return;

            // we store the username in the socket session for this client
            socket.first_name = data.first_name;
            socket.user_id = data.user_id;

            ++numUsers;
            addedUser = true;
            socket.emit('user_joined', {
                first_name: data.first_name,
                numUsers: numUsers
            });
        });

        // when the client emits 'typing', we broadcast it to others
        socket.on('typing', function () {
            socket.broadcast.emit('typing', {
                first_name: socket.first_name,
                user_id: socket.user_id
            });
        });

        // when the client emits 'stop typing', we broadcast it to others
        socket.on('stop_typing', function () {
            socket.broadcast.emit('stop_typing', {
                first_name: socket.first_name,
                user_id: socket.user_id
            });
        });

        // when the user disconnects.. perform this
        socket.on('disconnect', function () {
            if (addedUser) {
                --numUsers;
            }
        });
    });

    server.listen(app_config.CHAT_PORT);
}

