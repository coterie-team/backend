/**
 Created by rahilvora on 07/30/17
 */

'use strict';

let UserGroup = rootRequire('app/models/groupModels/UserGroup');
let GroupMember = rootRequire('app/models/groupMemberModels/GroupMember');
let Task = rootRequire('app/models/taskModels/Task');
let TaskQueue = rootRequire('app/models/taskModels/TaskQueue');
let TaskAssignedTo = rootRequire('app/models/taskModels/TaskAssignedTo');
let checkUtil = rootRequire('utils/checkUtil');
let taskScheduler = rootRequire('app/controllers/taskControllers/TaskScheduler');
let NotificationController = rootRequire('app/controllers/notificationController/NotificationController');
let ChatController = rootRequire('app/controllers/chatControllers/ChatController');

module.exports = {
    getUserGroups: {
        get(req, res){
            let user_id = req.params.user_id;
			console.log("Fetching groups for user_id: "+ user_id);
            GroupMember.findAndCountAll({
                include: [UserGroup],
                where: {user_id: user_id},
                attributes: ['group_id']
            }).then(groupsOfUser => {
                let groupData = [];
                for (let key in groupsOfUser.rows) {
                    groupData.push(groupsOfUser.rows[key].user_group);
                }
                //console.log(groupData);
                let data = {
                    groupCount: groupsOfUser.count,
                    groupData: groupData
                };
                let response = {
                    status: 200,
                    data: data,
                    msg: 'User group data'
                };
                res.send(response);

            })
        }
    },

    getGroupDetails: {
        get(req, res){
            UserGroup.findAll({
                where: {id: req.params.group_id}
            }).then(group => {
                if (group.length > 0) {
                    let response = {
                        group_data: group,
                        status: 200,
                        msg: 'group details sent'
                    };
                    res.send(response);
                } else {
                    let response = {
                        status: 200,
                        msg: 'group not exists'
                    };
                    res.send(response);
                }
            })
        }
    },

    addGroup: {
        post(req, res){
            let body = req.body;

            if (checkUtil.isEmpty(body)) {
                res.send({status_code: 400, msg: 'No message body found, bad request', data: {}});
                return;
            }

            if (checkUtil.isEmpty(body.name) || checkUtil.isEmpty(body.members) || checkUtil.isEmpty(body.added_by)) {
                res.send({
                    status_code: 400, msg: 'Bad request. One of the required param is not found. Required are ' +
                    '{name, members, added_by}', data: {}
                });
                return;
            }
            let membersArr = body.members;

            let group = {
                name: body.name,
                members: membersArr.length,
                added_by: body.added_by
            };

            UserGroup.create(group).then(userGroup => {
                addMembers(membersArr, userGroup).then(result => {
                    if(!result) {
                        res.send({status_code: 500, msg: 'Failed to add members to new group', data: userGroup});
                        return;
                    }

                    let user_group = {chat_space: userGroup.id + '_' + userGroup.name};
                    UserGroup.update(user_group,
                        {
                            where: {id: userGroup.id}
                        }
                    ).then(result => {
                        UserGroup.findOne({
                            where: {id: userGroup.id}
                        }).then(updateUserGroup => {
                            //NotificationController.sendGroupCreateNotification(membersArr, updateUserGroup);
                            ChatController.setChatSpace(updateUserGroup);
                            res.send({status_code: 200, msg: 'Success adding new group', data: updateUserGroup});
                        });
                    }).catch(error => {
                        console.log('Error updating chat space for group '+ error);
                        res.send({status_code: 500, msg: 'Failed adding new group', data: {}});
                    });
                }).catch(error => {
                    console.log('Error adding new group '+ error);
                    res.send({status_code: 500, msg: 'Failed adding new group', data: {}});
                })
            });
        }
    },

    updateGroup: {
        put(req, res){

            if(checkUtil.isEmpty(req.body) || checkUtil.isEmpty(req.params.group_id)) {
                return res.send({status_code: 400, msg: 'Bad request, no body or group_id', data: {}});
            }

            if(checkUtil.isEmpty(req.body.group_name) || checkUtil.isEmpty(req.body.members)) {
                return res.send({status_code: 400, msg: 'Bad request, no group name or members', data: {}});
            }

            let group_name = req.body.group_name;
            let new_members = req.body.members;
            let group_id = req.params.group_id;

            let is_new_members = false;
            let is_group_name_updated = false;
            let members_in_task = [];

            TaskQueue.findAll({
                where: {group_id: group_id}
            }).then(all_task_queue => {
                all_task_queue.map(task_queue => {
                    if(members_in_task.indexOf(task_queue.user_id) == -1) {
                        members_in_task.push(task_queue.user_id);
                    }
                });

                UserGroup.findOne({where: {id: group_id}}).then(db_user_group => {
                    if(checkUtil.isEmpty(db_user_group)) {
                        var response = {status_code: 404, msg: 'User group for group_id: ' + group_id + ' not found', data: {}};
                        res.send(response);
                        return;
                    }

                    if(members_in_task.length > 0 && members_in_task.length == new_members.length) {
                        for (let i = 0; i < new_members.length; i++) {
                            if (members_in_task.indexOf(parseInt(new_members[i])) == -1) {
                                is_new_members = true;
                            }
                        }
                    }


                    if(db_user_group.name != group_name) {
                        is_group_name_updated = true;
                    }

                    // If members updated is true, send message back that user is involved in task
                    if(is_new_members && !is_group_name_updated) {
                        res.send({status_code: 400, msg: 'Members of group are involved in task, please make changes to task before updating the group'
                            , data: {}});
                    }else {
                        let user_group = {name: group_name, members: new_members.length};
                        UserGroup.update(user_group, {where: {id: group_id}}).then(result => {
                            UserGroup.findOne({where: {id: group_id}}).then(userGroup => {

                                if(!is_new_members) {
                                    addMembers(new_members, userGroup).then(result => {
                                        var response = {status_code: 200, msg: 'Success updating group and members', data: userGroup};
                                        if(!result) {
                                            response = {status_code: 500, msg: 'Failed to update members to group', data: userGroup};
                                            res.send(response);
                                        }
                                        res.send(response);
                                    }).catch(error => {
                                        console.log('Error adding new group '+ error);
                                        var response = {status_code: 500, msg: 'Failed adding new group', data: {}};
                                        res.send(response);
                                    });
                                }else {
                                    res.send({status_code: 200, msg: 'Success updating group details only', data: userGroup});
                                }

                            });
                        });
                    }
                });
            });
        }

    },
    deleteGroup: {
        delete(req, res) {
            if(checkUtil.isEmpty(req.params.group_id)) {
                return res.send({status_code: 400, msg: 'Bad request: group_id not found', data:{}});
            }

            let group_id = req.params.group_id;
            TaskQueue.destroy({
                where: {
                    group_id: group_id
                },
                onDelete: 'cascade'
            }).then(userinvolved => {
                GroupMember.destroy({
                    where: {
                        group_id: group_id
                    },
                    onDelete: 'cascade'
                }).then(groupmember => {

                    TaskAssignedTo.destroy({
                        where: {
                            group_id: group_id
                        },
                        onDelete: 'cascade'
                    }).then(affectedRows => {
                        console.log("After task delete " + affectedRows);
                        Task.findAll({where: {group_id: group_id}}).then(allTasks => {
                            allTasks.map(task => {
                                if(task.schedule_id != '-1') {
                                    taskScheduler.deleteTaskSchedule(task);
                                }
                            });

                            Task.destroy({
                                where: {
                                    group_id: group_id
                                },
                                onDelete: 'cascade'
                            }).then(taskdestroyed => {
                                UserGroup.destroy({
                                    where: {
                                        id: group_id
                                    },
                                    onDelete: 'cascade'
                                }).then(groups =>{
                                    console.log("After task delete " + groups);
                                    if(groups == 1){
                                        res.send({status_code: 200, msg: 'Group deleted successfully', data: {}});
                                    } else {
                                        res.send({status_code: 400, msg: 'Group not found with group id', data: {}})
                                    }

                                }).catch(err => {
                                    console.log("Deleting group error " + err);
                                    let response = {
                                        status_code: 500,
                                        msg: 'Failed to delete the group'
                                    };
                                    res.send(response);
                                });

                            }).catch(err => {
                                console.log("Task for group delete error " + err);
                                let response = {
                                    status_code: 500,
                                    msg: 'Failed to delete Task associated with group'
                                };
                                res.send(response);
                            });
                        });
                    }).catch(err => {
                        console.log("Task assigned for group error " + err);
                        let response = {
                            status_code: 500,
                            msg: 'Failed to delete TaskAssignedTo associated with group'
                        };
                        res.send(response);
                    });
                }).catch( err => {
                    console.log("Group member delete error " + err);
                    let response = {
                        status_code: 500,
                        msg: 'Failed to delete GroupMember associated with group'
                    };
                    res.send(response);
                });
            }).catch( err => {
                console.log("Task queue for group delete error " + err);
                let response = {
                    status_code: 500,
                    msg: 'Failed to delete TaskQueue associated with group'
                };
                res.send(response);
            });
        }
    }
};

function addMembers(members_arr, user_group) {
    return new Promise(
        function (resolve, reject) {
            for(let i=0; i < members_arr.length; i++) {
                GroupMember.create({
                    user_id: members_arr[i],
                    group_id: user_group.id
                }).then(member_added => {
                    console.log('Member added');
                }).catch(error => {
                    reject(error);
                });
            }
            resolve(true);
        }
    )
}