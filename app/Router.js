/**
 Created by rahilvora on 07/30/17
 */

module.exports = [

    {
        path: '/groups-tasks/:group_id',
        handler: rootRequire('app/routes/taskRoute/TaskRoutes')
    },
    {
        path: '/groups-members',
        handler: rootRequire('app/routes/groupMemberRoute/GroupMembersRoutes'),
    },
    {
        path: '/users',
        handler: rootRequire('app/routes/userRoute/UserRoutes')
    },
    {
        path: '/groups',
        handler: rootRequire('app/routes/groupRoute/GroupRoutes')
    },
    {
        path: '/fcm-token',
        handler: rootRequire('app/routes/notificationRoute/NotificationRoutes')
    },
    {
        path: '/chat',
        handler: rootRequire('app/routes/chatRoute/ChatRoutes')
    },
    {
        path: '/images',
        handler: rootRequire('app/routes/photoRoute/PhotoRoutes')
    }
];
