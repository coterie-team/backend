/**
	Created by rahilvora on 07/30/17
*/

module.exports = function() {
	global.PROD_ENV = process.env.NODE_ENV && process.env.NODE_ENV === 'production';
};
