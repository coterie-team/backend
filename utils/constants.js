/**
	Created by rahilvora on 07/30/17
*/

'use strict';

let path = require('path');

let frequency = {
	1: 'Daily',
	2: 'Weekly',
	3: 'Bi-weekly',
	4: 'Monthly',
	5: 'Once',
	100: 'Test'
};

let task_type = {
	1: 'Monetary',
	2: 'Non-monetary'
};

let assign_type = {
	1: 'Alphabetical',
	2: 'Random'
};

let login_type = {
	1: 'Manual',
	2: 'Google',
	3: 'Facebook'
};

module.exports = {
	PATHS: {
		route: `${path.dirname(require.main.filename)}/../app/routes`,
		controller: `${path.dirname(require.main.filename)}/../app/controllers`,
		model: `${path.dirname(require.main.filename)}/../app/models`,
	},
};

module.exports.getFrequency = function(val){
	return frequency[val];
};

module.exports.getTaskType = function(val){
	return task_type[val];
};

module.exports.getAssignType = function(val){
	return assign_type[val];
};

module.exports.getLoginType = function(val){
	return login_type[val];
};