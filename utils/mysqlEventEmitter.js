/**
 * Created by rahilvora on 9/30/17.
 */

/*
*
*   1. This is mysqlEventEmitter module used to set sockets, emit and listen events w.r.t to various sockets
*
*   2. Currently it consist of implementations of task emit and listen event, similarly can be extend to accomodate other modules
*
*   3. Currently, socket is created for each module. Later on single socket can be used to and and listen to all diffrent event
*      based on scope and requirements.
*
* */
'use strict';
let emitterSocket;

const mysqlEventEmitter = {
    /*
    * @function setEmitterSocket
    *
    * @arguments
    *  1. socket: socket object
    *
    * @returns null*/
    setEmitterSocket(socket){
        emitterSocket = socket;
    },

    /*
    *  @function setEmitterSocket
    *
    *  @arguments
    *   1. eventName: Name of the event which is to be emitted
    * */

    emitTaskAssignedUpdate(eventName, data){
        emitterSocket.emit(eventName,{'Task assigned to' :data});
    },

    /*
     *  @function listenTaskEvents
     *
     *  @arguments
     *   1. eventName: Name of the event which is to be listed
     * */
    listenTaskEvents(eventName){
        emitterSocket.on(eventName, ()=>{
            //TODO : do someting when specific task event is listen
        })
    }

};
module.exports = mysqlEventEmitter;