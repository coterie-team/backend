/**
 * Created by rishi on 11/22/17.
 */

'use strict';

let multer = require('multer');

// To get more info about 'multer'.. you can go through https://www.npmjs.com/package/multer..
let storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now());
    }
});

let MulterUpload = multer({
    storage: storage
});

module.exports = MulterUpload;
