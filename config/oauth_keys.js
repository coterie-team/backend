/**
 * Created by Ishan on 8/3/2017.
 */

module.exports = {

    'googleAuth' : {
        'clientID'      : process.env.GOOGLE_CLIENT_ID, // your App ID
        'clientSecret'  : process.env.GOOGLE_SECRET, // your App Secret
        'callbackURL'   : process.env.GOOGLE_CALLBACK_URL
    },

    'fbAuth' : {
        'clientID'      : process.env.FB_CLIENT_ID,
        'clientSecret'  : process.env.FB_SECRET,
        'callbackURL'   : process.env.FB_CALLBACK_URL
    }

};
