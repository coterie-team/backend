/**
 * Created by rishi on 11/8/17.
 */

'use strict';
require('dotenv').config();

let firebase_admin = require("firebase-admin");
let serviceAccount = require(process.env.FIREBASE_JSON_NAME);

firebase_admin.initializeApp({
    credential: firebase_admin.credential.cert(serviceAccount),
    databaseURL: process.env.FIREBASE_DB_URL
});


module.exports = firebase_admin;