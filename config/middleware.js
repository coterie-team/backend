/**
 Created by rahilvora on 07/30/17
 */

'use strict';

let bodyParser = require('body-parser');
let methodOverride = require('method-override');
let path = require('path');
let session = require('express-session');
let compression = require('compression');
let passport = require('passport');

module.exports = function (app, express) {

    if (global.PROD_ENV) {
        app.use(compression());
    }

    /*
     * Parse JSON
     * app.use(bodyParser.json());
     **/

    app.use(bodyParser.urlencoded({
        extended: true,
    }));

    /*
     * Use PUT / DELETE HTTP verb
     * app.use(methodOverride());
     **/

    app.use(session({
        secret: 'sUperS3cr3t',
        saveUninitialized: true,
        resave: true,
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    require('./passport')(passport);
    app.set('views', path.join(__dirname, '../app/views'));
    app.set('view engine', 'ejs');
    app.use(express.static(path.join(__dirname, '../assets')));

    if (app.get('env') === 'development') {
        app.use(function (err, req, res, next) {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: err
            });
        });

    }

// production error handler
// no stacktraces leaked to user
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    });
};
