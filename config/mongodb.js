/**
 * Created by rahilvora on 11/18/17.
 */

'use strict';

let mongoose = require('mongoose');
mongoose.Promise = global.Promise;
require('dotenv').config();

// Could be moved to model dir
let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

let groupChat= new Schema({
    groupid   : Number,
    messages  : []
});


var promise = mongoose.connect(process.env.MONGODB_URL, {
    useMongoClient: true,
    /* other options */
});

promise.then(function (mongodb) {
    console.log('Connected to Mongodb successfully!');
    module.exports = mongodb;
});


