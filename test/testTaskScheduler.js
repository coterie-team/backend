/**
 * To run this test use nodeunit test/testTaskSchedule.js
 * Created by rishi on 10/28/17.
 */


'use strict';

var schedule = require('node-schedule');

// 12:30:15 pm Thursday 29 April 2010 in the timezone this code is being run in
// MONTH - range(0, 11)
// DAYSOFWEEK - range(0, 6)
// DATE - range(1, 31)
// HOUR - range(0, 23)

var base = new Date(2010, 3, 29, 12, 30, 15, 0);
var baseMs = base.getTime();

module.exports = {
    "#nextInvocationDate(Date)": {
        "WEEKLY schedule test": function (test) {
            var rule = new schedule.RecurrenceRule();
            // In actual code replace 0 with Day of week based on start date
            rule.dayOfWeek = 1;
            rule.hour = 0;
            rule.minute = 0;
            rule.month = new schedule.Range(0, 11);

            var next;

            next = rule.nextInvocationDate(base);
            test.deepEqual(new Date(2010, 4, 3, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 4, 10, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 4, 17, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 4, 24, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 4, 31, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 5, 7, 0, 0, 0, 0), next);

            test.done();
        },

        "DAILY schedule test": function(test) {
            var rule = new schedule.RecurrenceRule();
            // In actual code replace 0 with Day of week based on start date
            rule.dayOfWeek = [0, new schedule.Range(0, 6)];
            rule.hour = 0;
            rule.minute = 0;

            var next;

            next = rule.nextInvocationDate(base);
            test.deepEqual(new Date(2010, 3, 30, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 4, 1, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 4, 2, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 4, 3, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 4, 4, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 4, 5, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 4, 6, 0, 0, 0, 0), next);

            test.done();
        },

        "MONTHLY schedule test": function(test) {
            var rule = new schedule.RecurrenceRule();
            rule.month = new schedule.Range(0, 11);
            rule.hour = 0;
            rule.minute = 0;
            // In actual code replace, 1 with start date
            rule.date = 1;

            var next;

            next = rule.nextInvocationDate(base);
            test.deepEqual(new Date(2010, 4, 1, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 5, 1, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 6, 1, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 7, 1, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 8, 1, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 9, 1, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 10, 1, 0, 0, 0, 0), next);

            test.done();
        },

        "BIWEEKLY schedule test": function (test) {
            var rule = new schedule.RecurrenceRule();
            rule.hour = 0;
            rule.minute = 0;
            // Specifies range with 15 days gap
            // In actual code replace 1, with the start date
            rule.date = [1, new schedule.Range(1, 31, 15)];
            var next;

            next = rule.nextInvocationDate(base);
            test.deepEqual(new Date(2010, 4, 1, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 4, 16, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 5, 1, 0, 0, 0, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 5, 16, 0, 0, 0, 0), next);

            test.done();
        },

        "ONCE schedule test": function (test) {
            var rule = new schedule.RecurrenceRule();
            // Replace values with actual start date in code
            rule.hour = 12;
            rule.minute = 30;
            rule.date = 5;
            rule.month = 8;
            rule.year = 2017;

            var next = rule.nextInvocationDate(base);
            test.deepEqual(new Date(2017, 8, 5, 12, 30, 0, 0), next);
            test.done();
        },

        "next 5th second (minutes incremented)": function(test) {
            var rule = new schedule.RecurrenceRule();
            rule.second = 5;

            var next = rule.nextInvocationDate(base);
            test.deepEqual(new Date(2010, 3, 29, 12, 31, 5, 0), next);

            next = rule.nextInvocationDate(next);
            test.deepEqual(new Date(2010, 3, 29, 12, 32, 5, 0), next);

            test.done();
        },
    }
};