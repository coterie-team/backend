/**
 * Created by rahilvora on 8/31/17.
 */
'use strict';
let winston = require('winston');

let logger = new (winston.Logger)({
    transports:[
        new (winston.transports.File)({ filename:'logs/api.log'})
    ]
});

module.exports = logger;