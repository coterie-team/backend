/**
 * Created by rahilvora on 8/28/17.
 */
'use strict';
require('../utils/rootRequire')();
let db = rootRequire('config/db');
let User = rootRequire('app/models/userModels/User');
let Interest = rootRequire('app/models/userModels/Interest');
let Task = rootRequire('app/models/taskModels/Task');
let UserGroup = rootRequire('app/models/groupModels/UserGroup');
let GroupMember = rootRequire('app/models/groupMemberModels/GroupMember');
let TaskAssignedTo = rootRequire('app/models/taskModels/TaskAssignedTo');
let TaskQueue = rootRequire('app/models/taskModels/TaskQueue');

var bcrypt = require('bcrypt');
const saltRounds = 10;

let userData = [
    {
        id: 1,
        first_name: 'Rahil',
        last_name: 'Vora',
        email: 'rahil.vora740@gmail.com',
        password: bcrypt.hashSync('rahil123', saltRounds),
        phone_number: '6692389919',
        source: 'Manual'

    },
    {
        id: 2,
        first_name: 'Ishan',
        last_name: 'Pandya',
        email: 'ishan.pandya@gmail.com',
        password: bcrypt.hashSync('ishan123', saltRounds),
        phone_number: '6692389920',
        source: 'Manual'
    },
    {
        id: 3,
        first_name: 'Rishiraj',
        last_name: 'Randive',
        email: 'rishi.randive@gmail.com',
        password: bcrypt.hashSync('rishi123', saltRounds),
        phone_number: '6692389921',
        source: 'Manual'
    },
    {
        id: 4,
        first_name: 'Dhiraj',
        last_name: 'Gurnani',
        email: 'dhiraj.gurnani@gmail.com',
        password: bcrypt.hashSync('dhiraj123', saltRounds),
        phone_number: '6692389922',
        source: 'Manual'

    },
    {
        id: 5,
        first_name: 'Dan',
        last_name: 'Harkey',
        email: 'Dan.Harkey@gmail.com',
        password: bcrypt.hashSync('dan123', saltRounds),
        phone_number: '0934093434',
        photo_id: 1244,
        source: 'Manual'

    },
    {
        id: 6,
        first_name: 'Shim',
        last_name: 'Shimon',
        email: 'shim.shimon@gmail.com',
        password: bcrypt.hashSync('shim123', saltRounds),
        phone_number: '09309403493',
        source: 'Manual'

    },
    {
        id: 7,
        first_name: 'Shim',
        last_name: 'Zhang',
        email: 'shim.zhang@gmail.com',
        password: bcrypt.hashSync('charles123', saltRounds),
        phone_number: '98494545',
        source: 'Manual'

    }
];

let userGroupData = [
    {
        id: 1,
        name: '101 San Fernando',
        photo_id: 1234,
        members: 2,
        added_by: 1,
        chat_space: '1_101 San Fernando'
    },
    {
        id: 2,
        name: 'Villa Torino',
        photo_id: 1235,
        members: 2,
        added_by: 3,
        chat_space: '2_Villa Torino'
    },
    {
        id: 3,
        name: 'Road trip',
        photo_id: 1236,
        members: 2,
        added_by: 2,
        chat_space: '3_Road trip'
    },
    {
        id: 4,
        name: 'Avalon Alameda',
        photo_id: 1240,
        members: 1,
        added_by: 2,
        chat_space: '4_Avalon Alameda'
    }
];

let taskData = [
    {
        id: 1,
        name: 'Cleaning',
        frequency: 'Daily',
        type: 'Non-monetary',
        group_id: 2,
        added_by: 3
    },
    {
        id: 2,
        name: 'Cooking',
        frequency: 'Daily',
        type: 'Non-monetary',
        group_id: 2,
        added_by: 4
    },
    {
        id: 3,
        name: 'Bill payment',
        frequency: 'Monthly',
        type: 'Monetary',
        group_id: 1,
        added_by: 1
    },
    {
        id: 4,
        name: 'Trash',
        frequency: 'Weekly',
        type: 'Non-Monetary',
        group_id: 1,
        added_by: 2
    },
    {
        id: 5,
        name: 'Shopping',
        frequency: 'Once',
        type: 'Monetary',
        group_id: 3,
        added_by: 3
    },
];

let groupMemberData = [
    {
        user_id: 1,
        group_id: 1
    },
    {
        user_id: 2,
        group_id: 1
    },
    {
        user_id: 3,
        group_id: 2
    },
    {
        user_id: 4,
        group_id: 2
    },
    {
        user_id: 2,
        group_id: 3
    },
    {
        user_id: 3,
        group_id: 3
    }
];

let interestData = [
    {
        user_id: 1,
        activity: 'Hiking'
    },
    {
        user_id: 2,
        activity: 'Hiking'
    },
    {
        user_id: 1,
        activity: 'Gym'
    }
];

let taskAssignedToData = [
    {
        group_id: 2,
        task_id: 1,
        assigned_user_id: 3,
        completed: true,
        assign_seq: 0
    },
    {
        group_id: 2,
        task_id: 2,
        assigned_user_id: 4,
        assign_seq: 0
    },
    {
        group_id: 1,
        task_id: 3,
        assigned_user_id: 1,
        assign_seq: 0,
        completed: true
    },
    {
        group_id: 1,
        task_id: 4,
        assigned_user_id: 2,
        assign_seq: 0
    },
    {
        group_id: 3,
        task_id: 5,
        assigned_user_id: 3,
        assign_seq: 0
    }
];

let taskQueueData = [
    {
        group_id: 2,
        task_id: 1,
        user_id: 3,
        user_seq: 0
    },
    {
        group_id: 2,
        task_id: 1,
        user_id: 4,
        user_seq: 1
    },
    {
        group_id: 2,
        task_id: 2,
        user_id: 4,
        user_seq: 0
    },
    {
        group_id: 1,
        task_id: 3,
        user_id: 1,
        user_seq: 0
    },
    {
        group_id: 1,
        task_id: 3,
        user_id: 2,
        user_seq: 1
    },
    {
        group_id: 1,
        task_id: 4,
        user_id: 2,
        user_seq: 0
    },
    {
        group_id: 1,
        task_id: 4,
        user_id: 1,
        user_seq: 1
    },
    {
        group_id: 3,
        task_id: 5,
        user_id: 3,
        user_seq: 0
    },
    {
        group_id: 3,
        task_id: 5,
        user_id: 2,
        user_seq: 1
    }
];

User.bulkCreate(userData).then(() => {
    return User.findAll();
}).then(user => {
    console.log('User data entered');

    UserGroup.bulkCreate(userGroupData).then(() => {
        return UserGroup.findAll();
    }).then(usergroup => {
        console.log('UserGroup data entered');

        Task.bulkCreate(taskData).then(() => {
            return Task.findAll();
        }).then(task => {
            console.log('Task data entered');

            GroupMember.bulkCreate(groupMemberData).then(() => {
                return GroupMember.findAll();
            }).then(groupMember => {
                console.log('GroupMember data entered');

                TaskAssignedTo.bulkCreate(taskAssignedToData).then(() => {
                    return TaskAssignedTo.findAll();
                }).then(taskAssignedTo => {
                    console.log('TaskAssignedTo data entered');
                });

                TaskQueue.bulkCreate(taskQueueData).then(() => {
                    return TaskQueue.findAll();
                }).then(taskQueue => {
                    console.log('TaskQueue data entered');
                });
            });
        });
    });
});

Interest.bulkCreate(interestData).then(() => {
    return Interest.findAll();
}).then(interest => {
    console.log('Interest data entered');
});