/**
	Created by rahilvora on 07/30/17
*/

'use strict';

require('../utils/rootRequire')();

let db = rootRequire('config/db');
let User = rootRequire('app/models/userModels/User');
let Interest = rootRequire('app/models/userModels/Interest');
let Task = rootRequire('app/models/taskModels/Task');
let UserGroup = rootRequire('app/models/groupModels/UserGroup');
let GroupMember = rootRequire('app/models/groupMemberModels/GroupMember');
let TaskAssignedTo = rootRequire('app/models/taskModels/TaskAssignedTo');
let TaskQueue = rootRequire('app/models/taskModels/TaskQueue');

User.hasMany(Interest, {foreignKey: 'user_id'});

User.hasMany(GroupMember, {foreignKey: 'user_id', onDelete: 'CASCADE', hooks: true});
User.hasMany(TaskAssignedTo, {foreignKey: 'assigned_user_id' , onDelete: 'CASCADE', hooks: true});

User.hasMany(TaskQueue, {foreignKey: 'user_id'});

UserGroup.hasMany(Task, {foreignKey: 'group_id',onDelete: 'CASCADE', hooks: true});
UserGroup.hasMany(GroupMember, {foreignKey: 'group_id', onDelete: 'CASCADE', hooks: true});
UserGroup.hasMany(TaskAssignedTo, {foreignKey: 'group_id' , onDelete: 'CASCADE', hooks: true});
UserGroup.hasMany(TaskQueue, {foreignKey: 'group_id'});

Task.hasMany(TaskAssignedTo, {foreignKey: 'task_id', onDelete: 'CASCADE', hooks: true});
Task.hasMany(TaskQueue, {foreignKey: 'task_id'});

db.sync();
