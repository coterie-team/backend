/**
 * Created by rahilvora on 8/28/17.
 */
'use strict';
require('../utils/rootRequire')();
let db = rootRequire('config/db');
let User = rootRequire('app/models/userModels/User');
let Interest = rootRequire('app/models/userModels/Interest');
let Task = rootRequire('app/models/taskModels/Task');
let UserGroup = rootRequire('app/models/groupModels/UserGroup');
let GroupMember = rootRequire('app/models/groupMemberModels/GroupMember');
let TaskAssignedTo = rootRequire('app/models/taskModels/TaskAssignedTo');
let TaskQueue = rootRequire('app/models/taskModels/TaskQueue');

TaskQueue.drop().then(() => {
    TaskAssignedTo.drop().then(() => {
        Task.drop().then(() => {
            UserGroup.drop();
            User.drop();
        });
    });
});
GroupMember.drop();
Interest.drop();
