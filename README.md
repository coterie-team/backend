# backend

Tables:

GroupMember:
User_id,
Group_id

UserGroup:
Id,
Name,
Photo_id,
Total_no_of_members

Interest:
Activity,
User_id

User:
Id,
Name,
Email,
Password,
Phone_number,
Photo_url,
source,

Task:
Id,
Name,
Frequency,
Start_date,
Type,
Group_id

TaskAssignedTo:
Assigned_date,
Due_date,
Completed,
Assigned_user_id,
Group_id,
Task_id

UserInvovledTask:
User_id,
Group_id,
Task_id

